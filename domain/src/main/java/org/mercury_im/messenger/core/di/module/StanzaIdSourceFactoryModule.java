package org.mercury_im.messenger.core.di.module;

import org.jivesoftware.smack.packet.id.StandardStanzaIdSource;
import org.jivesoftware.smack.packet.id.StanzaIdSourceFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StanzaIdSourceFactoryModule {

    @Provides
    @Singleton
    static StanzaIdSourceFactory provideStanzaIdSourceFactory() {
        return new StandardStanzaIdSource.Factory();
    }
}
