package org.mercury_im.messenger.core.listener;

import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.entity.chat.GroupChat;
import org.mercury_im.messenger.entity.message.Message;

public interface IncomingGroupChatMessageListener {

    void onIncomingDirectMessage(Account account, GroupChat chat, Message message);

}
