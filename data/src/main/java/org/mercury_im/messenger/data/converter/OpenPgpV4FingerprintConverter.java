package org.mercury_im.messenger.data.converter;

import org.pgpainless.key.OpenPgpV4Fingerprint;

import io.requery.Converter;

public class OpenPgpV4FingerprintConverter implements Converter<OpenPgpV4Fingerprint, String> {
    @Override
    public Class<OpenPgpV4Fingerprint> getMappedType() {
        return OpenPgpV4Fingerprint.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @javax.annotation.Nullable
    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(OpenPgpV4Fingerprint value) {
        return value == null ? null : value.toString();
    }

    @Override
    public OpenPgpV4Fingerprint convertToMapped(Class<? extends OpenPgpV4Fingerprint> type, @javax.annotation.Nullable String value) {
        return value == null ? null : new OpenPgpV4Fingerprint(value);
    }
}
