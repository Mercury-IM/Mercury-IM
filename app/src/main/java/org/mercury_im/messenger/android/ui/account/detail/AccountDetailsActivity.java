package org.mercury_im.messenger.android.ui.account.detail;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.ui.account.login.IkeyBackupCreationFragment;
import org.mercury_im.messenger.android.ui.account.login.IkeyBackupRestoreOrSkipFragment;
import org.mercury_im.messenger.android.ui.account.login.IkeyKeyInfoFragment;
import org.mercury_im.messenger.android.ui.account.login.IkeySetupNavigator;
import org.mercury_im.messenger.android.ui.base.MercuryActivity;
import org.mercury_im.messenger.android.util.ArgumentUtils;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Value;

public class AccountDetailsActivity extends AppCompatActivity implements MercuryActivity, IkeySetupNavigator {

    public static final String EXTRA_ACCOUNT_ID = "ACCOUNT_ID";

    private UUID accountId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //MercuryImApplication.getApplication().getAppComponent().inject(this);

        Arguments arguments = getArguments(savedInstanceState);
        accountId = arguments.getAccountId();

        bindUiComponents();
    }

    private void bindUiComponents() {
        setContentView(R.layout.layout_top_toolbar);
        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, new AccountDetailsFragment(accountId), "account_details")
                .commit();

        setSupportActionBar(toolbar);
    }

    private Arguments getArguments(Bundle bundle) {
        bundle = requiredExtras(bundle, getIntent());
        UUID accountId = ArgumentUtils.requireUUID(bundle, EXTRA_ACCOUNT_ID);
        return new Arguments(accountId);
    }

    @Override
    public void setupIkey(UUID accountId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, IkeyBackupRestoreOrSkipFragment.newInstance(accountId), "setup_ikey")
                .commit();
    }

    @Override
    public void restoreSuccessful(UUID accountId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, IkeyKeyInfoFragment.newInstance(accountId), "ikey_success")
                .commit();
    }

    @Override
    public void generateIkeyBackup(UUID accountId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, IkeyBackupCreationFragment.newInstance(accountId), "backup")
                .commit();
    }

    @Override
    public void displayInfo(UUID accountId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment,  IkeyKeyInfoFragment.newInstance(accountId), "info")
                .commit();
    }

    @Value
    private class Arguments {
        UUID accountId;
    }
}
