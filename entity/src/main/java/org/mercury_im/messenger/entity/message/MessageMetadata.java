package org.mercury_im.messenger.entity.message;

import lombok.Data;

/**
 * Interface to allow additional, protocol specific metadata to be attached to the message.
 * In case of XMPP this might be origin/stanza id, encryption information etc.
 */
@Data
public class MessageMetadata {
    private long id;

}