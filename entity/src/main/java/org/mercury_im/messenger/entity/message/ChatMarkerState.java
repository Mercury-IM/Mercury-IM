package org.mercury_im.messenger.entity.message;

public enum ChatMarkerState {
    markable,
    received,
    displayed,
    acknowledged
}
