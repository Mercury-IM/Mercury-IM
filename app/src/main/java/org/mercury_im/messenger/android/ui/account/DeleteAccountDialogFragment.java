package org.mercury_im.messenger.android.ui.account;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.lifecycle.ViewModelProvider;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.ui.account.list.AndroidAccountListViewModel;

import java.util.UUID;

public class DeleteAccountDialogFragment extends AppCompatDialogFragment {

    private AndroidAccountListViewModel viewModel;
    private final UUID accountId;

    public DeleteAccountDialogFragment(UUID accountId) {
        this.accountId = accountId;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(requireActivity()).get(AndroidAccountListViewModel.class);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Do you really want to delete this account?")
                .setPositiveButton(R.string.button_delete, (dialog, id) -> viewModel.onDeleteAccount(accountId))
                .setNegativeButton(R.string.button_cancel, (dialog, id) -> dialog.dismiss());
        return builder.create();
    }
}
