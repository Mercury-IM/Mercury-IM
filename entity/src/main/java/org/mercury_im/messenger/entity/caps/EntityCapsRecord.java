package org.mercury_im.messenger.entity.caps;

import lombok.Data;

@Data
public class EntityCapsRecord {
    String nodeVer;
    String xml;
}
