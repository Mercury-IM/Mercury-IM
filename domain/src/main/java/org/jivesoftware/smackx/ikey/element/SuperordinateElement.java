package org.jivesoftware.smackx.ikey.element;

import org.jivesoftware.smack.packet.NamedElement;
import org.jivesoftware.smack.packet.XmlEnvironment;
import org.jivesoftware.smack.util.EqualsUtil;
import org.jivesoftware.smack.util.HashCode;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smack.util.stringencoder.Base64;

public class SuperordinateElement implements NamedElement {

    public static final String ELEMENT = "superordinate";

    private final byte[] pubKeyBytes;

    public SuperordinateElement(byte[] bytes) {
        this.pubKeyBytes = bytes;
    }

    public SuperordinateElement(String base64) {
        this.pubKeyBytes = Base64.decode(base64);
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    public byte[] getPubKeyBytes() {
        return pubKeyBytes.clone();
    }

    public String getBase64PubKey() {
        return Base64.encodeToString(getPubKeyBytes());
    }

    @Override
    public XmlStringBuilder toXML(XmlEnvironment xmlEnvironment) {
        return new XmlStringBuilder(this)
                .rightAngleBracket()
                .append(getBase64PubKey())
                .closeElement(this);
    }

    @Override
    public int hashCode() {
        return HashCode.builder()
                .append(getElementName())
                .append(getBase64PubKey())
                .build();
    }

    @Override
    public boolean equals(Object other) {
        return EqualsUtil.equals(this, other, (e, o) -> e
                .append(getElementName(), o.getElementName())
                .append(getBase64PubKey(), o.getBase64PubKey()));
    }
}
