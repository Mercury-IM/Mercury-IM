package org.mercury_im.messenger.android.ui.contacts.detail;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.ui.base.MercuryActivity;
import org.mercury_im.messenger.android.util.ArgumentUtils;

import java.util.UUID;

import butterknife.ButterKnife;
import lombok.Value;

public class ContactDetailActivity extends AppCompatActivity implements MercuryActivity {
    public static final String EXTRA_PEER_ID = "PEER_ID";

    private ContactDetailViewModel androidContactDetailViewModel;
    private UUID peerId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MercuryImApplication.getApplication().getAppComponent().inject(this);

        Arguments arguments = getArguments(savedInstanceState);
        peerId = arguments.getPeerId();

        androidContactDetailViewModel = new ViewModelProvider(this).get(ContactDetailViewModel.class);
        androidContactDetailViewModel.init(peerId);

        bindUiComponents();
    }

    private void bindUiComponents() {
        setContentView(R.layout.layout_top_toolbar);
        ButterKnife.bind(this);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, new ContactDetailFragment(), "contact_details")
                .commit();
    }

    private Arguments getArguments(Bundle bundle) {
        bundle = requiredExtras(bundle, getIntent());
        UUID peerId = ArgumentUtils.requireUUID(bundle, EXTRA_PEER_ID);
        return new Arguments(peerId);
    }

    @Value
    private class Arguments {
        UUID peerId;
    }
}
