package org.mercury_im.messenger.entity.chat;

import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.impl.JidCreate;
import org.mercury_im.messenger.entity.Account;

import java.util.UUID;

import lombok.Data;

/**
 * Generic interface defining shared properties of chats.
 *
 * Child interfaces of {@link Chat} are {@link DirectChat} and {@link GroupChat}.
 */
@Data
public abstract class Chat {
    UUID id;
    Account account;
    ChatPreferences chatPreferences = new ChatPreferences();

    public Chat() {
        this.id = UUID.randomUUID();
    }

    public abstract String getAddress();

    public EntityJid getJid() {
        return JidCreate.entityFromOrThrowUnchecked(getAddress());
    }
}
