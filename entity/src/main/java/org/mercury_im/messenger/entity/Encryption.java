package org.mercury_im.messenger.entity;

public enum Encryption {
    plain,
    ox_sign,
    ox_crypt,
    ox_signcrypt,
    ;
}
