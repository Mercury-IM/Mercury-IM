package org.mercury_im.messenger.data.model;

import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;
import org.mercury_im.messenger.data.converter.Base64PGPSecretKeyRingConverter;
import org.mercury_im.messenger.data.converter.OpenPGPSecretKeyBackupPassphraseConverter;
import org.mercury_im.messenger.data.converter.OpenPgpV4FingerprintConverter;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Key;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Table(name = "ikey_sec_key")
@Entity
public class AbstractIkeySecretKeyModel {

    @Key
    @Convert(UUIDConverter.class)
    @Column(name = "account")
    UUID accountId;

    @Column(name = "fingerprint")
    @Convert(OpenPgpV4FingerprintConverter.class)
    OpenPgpV4Fingerprint fingerprint;

    @Column(name = "backup_passphrase")
    @Convert(OpenPGPSecretKeyBackupPassphraseConverter.class)
    OpenPgpSecretKeyBackupPassphrase backupPassphrase;

    @Column(name = "key")
    @Convert(Base64PGPSecretKeyRingConverter.class)
    PGPSecretKeyRing key;

    @Column(name = "trusted")
    boolean trusted;
}
