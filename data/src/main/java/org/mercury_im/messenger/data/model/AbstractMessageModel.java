package org.mercury_im.messenger.data.model;

import org.jxmpp.jid.EntityJid;
import org.mercury_im.messenger.data.converter.EntityJidConverter;
import org.mercury_im.messenger.data.converter.MessageDirectionConverter;
import org.mercury_im.messenger.entity.Encryption;
import org.mercury_im.messenger.entity.message.ChatMarkerState;
import org.mercury_im.messenger.entity.message.MessageDeliveryState;
import org.mercury_im.messenger.entity.message.MessageDirection;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.Date;
import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Entity
@Table(name = "messages")
public abstract class AbstractMessageModel implements Persistable {

    @Key
    @Convert(UUIDConverter.class)
    UUID id;

    @Column(nullable = false)
    @Convert(UUIDConverter.class)
    UUID chatId;

    @Column(nullable = false)
    @Convert(EntityJidConverter.class)
    EntityJid sender;

    @Column(nullable = false)
    @Convert(EntityJidConverter.class)
    EntityJid recipient;

    @Column(name = "\"timestamp\"", nullable = false)
    Date timestamp;

    @Column(nullable = false)
    @Convert(MessageDirectionConverter.class)
    MessageDirection direction;

    @Column(length = 65536)
    String body;

    @Column(length = 65536)
    String xml;

    @Column
    String legacyId;

    @Column(unique = true)
    String originId;

    @Column
    String stanzaId;

    @Column
    String stanzaIdBy;

    @Column
    Encryption encryption;

    @Column
    OpenPgpV4Fingerprint senderOXFingerprint;

    @Column
    ChatMarkerState chatMarkerState;

    @Column
    MessageDeliveryState deliveryState;

}
