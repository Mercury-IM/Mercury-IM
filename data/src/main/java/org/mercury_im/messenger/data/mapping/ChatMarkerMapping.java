package org.mercury_im.messenger.data.mapping;

import org.jivesoftware.smackx.chat_markers.ChatMarkersState;
import org.mercury_im.messenger.entity.message.ChatMarkerState;

public class ChatMarkerMapping {

    public static ChatMarkersState toSmackChatMarker(ChatMarkerState mercuryChatMarker) {
        switch (mercuryChatMarker) {
            case markable:
                return ChatMarkersState.markable;
            case received:
                return ChatMarkersState.received;
            case displayed:
                return ChatMarkersState.displayed;
            case acknowledged:
                return ChatMarkersState.acknowledged;
            default:
                throw new IllegalArgumentException("Illegal ChatMarkersState: " + mercuryChatMarker);
        }
    }

    public static ChatMarkerState toMercuryChatMarker(ChatMarkersState smackChatMarker) {
        switch (smackChatMarker) {
            case markable:
                return ChatMarkerState.markable;
            case received:
                return ChatMarkerState.received;
            case displayed:
                return ChatMarkerState.displayed;
            case acknowledged:
                return ChatMarkerState.acknowledged;
            default:
                throw new IllegalArgumentException("Illegal ChatMarkersState: " + smackChatMarker);
        }
    }
}
