package org.mercury_im.messenger.core.crypto;

import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;

/**
 * For testing purposes we always use the same static passphrase for secret key backups.
 * This obviously MUST NOT make it to production!
 */
public class InsecureStaticSecretKeyBackupPassphraseGenerator implements OpenPgpSecretKeyBackupPassphraseGenerator {

    @Override
    public OpenPgpSecretKeyBackupPassphrase generateBackupPassphrase() {
        return new OpenPgpSecretKeyBackupPassphrase("71ZA-Y416-UA7A-7NCE-3SNM-88EF");
    }

}
