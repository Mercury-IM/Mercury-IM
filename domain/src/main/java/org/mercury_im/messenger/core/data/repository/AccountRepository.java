package org.mercury_im.messenger.core.data.repository;

import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.entity.Account;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface AccountRepository {

    Single<Account> insertAccount(Account account);

    default Observable<Optional<Account>> observeAccount(Account account) {
        return observeAccount(account.getId());
    }

    Observable<Optional<Account>> observeAccount(UUID accountId);

    default Maybe<Account> getAccount(Account account) {
        return getAccount(account.getId());
    }

    Maybe<Account> getAccount(UUID accountId);

    Observable<Optional<Account>> observeAccountByAddress(String address);

    Maybe<Account> getAccountByAddress(String address);

    Observable<List<Account>> observeAllAccounts();

    Observable<Account> observeAccounts();

    Single<Account> updateAccount(Account account);

    Single<Account> upsertAccount(Account account);

    default Completable deleteAccount(Account account) {
        return deleteAccount(account.getId());
    }

    Completable deleteAccount(UUID accountId);

    default Completable updateRosterVersion(Account account, String rosterVersion) {
        return updateRosterVersion(account.getId(), rosterVersion);
    }

    Completable updateRosterVersion(UUID accountId, String rosterVersion);

}
