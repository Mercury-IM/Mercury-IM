package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.crypto.OpenPgpSecretKeyBackupPassphraseGenerator;
import org.mercury_im.messenger.core.crypto.SecureRandomSecretKeyBackupPassphraseGenerator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class OpenPgpModule {

    @Singleton
    @Provides
    static OpenPgpSecretKeyBackupPassphraseGenerator provideSecretKeyBackupPassphraseGenerator() {
        return new SecureRandomSecretKeyBackupPassphraseGenerator();
    }
}
