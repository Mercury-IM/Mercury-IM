package org.mercury_im.messenger.android.ui.chat.util;

import org.mercury_im.messenger.R;

/**
 * Enum that describes which position in a message grouping a message has.
 */
public enum MessageBackgroundDrawable {
    FIRST(R.drawable.bg_msg_in_first, R.drawable.bg_msg_out_first),
    MID(R.drawable.bg_msg_in_mid, R.drawable.bg_msg_out_mid),
    LAST(R.drawable.bg_msg_in_last, R.drawable.bg_msg_out_last),
    SINGLE(R.drawable.bg_msg_single, R.drawable.bg_msg_single),
    ;

    private final int in, out;

    MessageBackgroundDrawable(int in, int out) {
        this.in = in;
        this.out = out;
    }

    /**
     * Return the resource id for the drawable that is used to render the messages background
     * in case of an incoming message.
     * @return res id of incoming message background
     */
    public int getIncomingDrawable() {
        return in;
    }

    /**
     * Return the resource id for the drawable that is used to render the messages background
     * in case of an outgoing message.
     * @return res id of outgoing message background
     */
    public int getOutgoingDrawable() {
        return out;
    }
}
