package org.mercury_im.messenger.core.connection.state;

import org.mercury_im.messenger.core.connection.MercuryConnection;

import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.With;

@Value
@ToString
@EqualsAndHashCode
public class ConnectionState {

    UUID id;
    MercuryConnection connection;
    @With
    ConnectivityState connectivity;
    @With
    boolean resumed;

    public ConnectionState(UUID id, MercuryConnection connection, ConnectivityState connectivity, boolean resumed) {
        this.id = id;
        this.connection = connection;
        this.connectivity = connectivity;
        this.resumed = resumed;
    }

    public ConnectionState(UUID accountId, MercuryConnection connection) {
        this(accountId, connection, ConnectivityState.disconnected, false);
    }
}
