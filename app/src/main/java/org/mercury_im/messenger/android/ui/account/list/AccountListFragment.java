package org.mercury_im.messenger.android.ui.account.list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.ui.account.OnAccountListItemClickListener;
import org.mercury_im.messenger.android.ui.account.login.AddAccountActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnAccountListItemClickListener}
 * interface.
 */
public class AccountListFragment extends Fragment implements SearchView.OnQueryTextListener {

    private OnAccountListItemClickListener accountClickListener;

    AndroidAccountListViewModel viewModel;

    private AccountListRecyclerViewAdapter adapter;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    ExtendedFloatingActionButton fab;

    public AccountListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(AndroidAccountListViewModel.class);
        this.adapter = new AccountListRecyclerViewAdapter(viewModel, accountClickListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        observeViewModel();
        //fab.setOnClickListener(v -> displayAddAccountDialog());
        fab.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), AddAccountActivity.class));
        });
    }

    private void observeViewModel() {
        viewModel.getAccounts().observe(this,
                accounts -> adapter.setValues(new ArrayList<>(accounts)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_list, container, false);
        ButterKnife.bind(this, view);

        Context context = view.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAccountListItemClickListener) {
            accountClickListener = (OnAccountListItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAccountListItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        accountClickListener = null;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
