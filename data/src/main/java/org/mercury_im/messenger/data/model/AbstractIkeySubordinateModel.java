package org.mercury_im.messenger.data.model;

import java.net.URI;
import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Table;
import io.requery.converter.URIConverter;

@Table(name = "ikey_subordinates")
@Entity
public class AbstractIkeySubordinateModel {

    @Key
    UUID id;

    @Column(name = "type")
    String type;

    @Column(name = "uri")
    @Convert(URIConverter.class)
    URI uri;

    @Column(name = "fpr")
    String fpr;

    @ManyToOne
    AbstractIkeyRecordModel record;
}
