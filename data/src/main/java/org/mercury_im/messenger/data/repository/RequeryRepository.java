package org.mercury_im.messenger.data.repository;

import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

public abstract class RequeryRepository {

    private final ReactiveEntityStore<Persistable> data;

    protected RequeryRepository(ReactiveEntityStore<Persistable> data) {
        this.data = data;
    }

    protected ReactiveEntityStore<Persistable> data() {
        return data;
    }
}
