package org.mercury_im.messenger.di;

import android.content.Context;

import androidx.test.InstrumentationRegistry;

import org.mercury_im.messenger.data.model.Models;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

@Module
public class TestDatabaseModule {

    @Provides
    @Singleton
    public static ReactiveEntityStore<Persistable> provideDataStore() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        DatabaseSource source = new DatabaseSource(appContext, Models.DEFAULT,
                "mercury_test_db", 1);
        // use this in development mode to drop and recreate the tables on every upgrade
        source.setTableCreationMode(TableCreationMode.DROP_CREATE);
        source.setLoggingEnabled(true);
        Configuration configuration = source.getConfiguration();
        return ReactiveSupport.toReactiveStore(new EntityDataStore<>(configuration));
    }

}
