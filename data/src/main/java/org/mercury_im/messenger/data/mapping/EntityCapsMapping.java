package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.data.model.EntityCapsModel;
import org.mercury_im.messenger.entity.caps.EntityCapsRecord;

import javax.inject.Inject;

import lombok.NonNull;

public class EntityCapsMapping extends AbstractMapping<EntityCapsRecord, EntityCapsModel> {

    @Inject
    public EntityCapsMapping() {

    }

    @Override
    protected EntityCapsRecord newEntity(@NonNull EntityCapsModel model) {
        return new EntityCapsRecord();
    }

    @Override
    protected EntityCapsModel newModel(@NonNull EntityCapsRecord entity) {
        return new EntityCapsModel();
    }

    @Override
    protected EntityCapsModel mapToModel(@NonNull EntityCapsRecord entity, @NonNull EntityCapsModel model) {
        model.setNodeVer(entity.getNodeVer());
        model.setXml(entity.getXml());
        return model;
    }

    @Override
    protected EntityCapsRecord mapToEntity(@NonNull EntityCapsModel model, @NonNull EntityCapsRecord entity) {
        entity.setNodeVer(model.getNodeVer());
        entity.setXml(model.getXml());
        return entity;
    }
}
