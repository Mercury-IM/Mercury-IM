package org.mercury_im.messenger.core.util;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;

public class AppendCompletableToSingle<T> implements SingleTransformer<T, T> {

    private final Completable completable;

    public AppendCompletableToSingle(Completable completable) {
        this.completable = completable;
    }

    @Override
    public SingleSource<T> apply(Single<T> upstream) {
        return upstream.flatMap(result -> completable.toSingle(() -> result));
    }
}
