package org.mercury_im.messenger.android.util;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

public abstract class AbstractDiffCallback<I> extends DiffUtil.Callback {

    protected final List<I> newItems;
    protected final List<I> oldItems;

    public AbstractDiffCallback(List<I> newItems, List<I> oldItems) {
        this.newItems = newItems;
        this.oldItems = oldItems;
    }

    @Override
    public int getOldListSize() {
        return oldItems.size();
    }

    @Override
    public int getNewListSize() {
        return newItems.size();
    }
}
