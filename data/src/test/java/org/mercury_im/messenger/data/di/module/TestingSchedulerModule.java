package org.mercury_im.messenger.data.di.module;

import org.mercury_im.messenger.core.SchedulersFacade;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@Module
public class TestingSchedulerModule {

    @Provides
    @Singleton
    @Named(value = SchedulersFacade.SCHEDULER_IO)
    public static Scheduler provideSubscriberScheduler() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @Named(value = SchedulersFacade.SCHEDULER_UI)
    public static Scheduler provideObserverScheduler() {
        return Schedulers.trampoline();
    }
}
