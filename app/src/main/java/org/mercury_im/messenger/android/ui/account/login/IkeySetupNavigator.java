package org.mercury_im.messenger.android.ui.account.login;

import java.util.UUID;

public interface IkeySetupNavigator {

    void setupIkey(UUID accountId);

    void restoreSuccessful(UUID accountId);

    void generateIkeyBackup(UUID accountId);

    void displayInfo(UUID accountId);
}
