package org.mercury_im.messenger.core.util;

import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.PresenceEventListener;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.FullJid;
import org.jxmpp.jid.Jid;

public abstract class CombinedPresenceListener implements PresenceEventListener {
    @Override
    public void presenceAvailable(FullJid address, Presence availablePresence) {
        presenceReceived(address, availablePresence);
    }

    @Override
    public void presenceUnavailable(FullJid address, Presence presence) {
        presenceReceived(address, presence);
    }

    @Override
    public void presenceError(Jid address, Presence errorPresence) {
        presenceReceived(address, errorPresence);
    }

    @Override
    public void presenceSubscribed(BareJid address, Presence subscribedPresence) {
        presenceReceived(address, subscribedPresence);
    }

    @Override
    public void presenceUnsubscribed(BareJid address, Presence unsubscribedPresence) {
        presenceReceived(address, unsubscribedPresence);
    }

    public abstract void presenceReceived(Jid address, Presence presence);
}
