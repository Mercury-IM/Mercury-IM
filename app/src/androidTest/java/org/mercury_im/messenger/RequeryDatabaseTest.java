package org.mercury_im.messenger;

import androidx.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mercury_im.messenger.core.data.repository.AccountRepository;
import org.mercury_im.messenger.di.DaggerAndroidTestComponent;
import org.mercury_im.messenger.entity.Account;

import javax.inject.Inject;

import io.reactivex.observers.TestObserver;

@RunWith(AndroidJUnit4.class)
public class RequeryDatabaseTest {

    @Inject
    AccountRepository accountRepository;

    @Before
    public void setup() {
        DaggerAndroidTestComponent.builder().build().inject(this);
    }

    @Test
    public void insertQueryDeleteAccountTest() {
        Account account = new Account();
        account.setEnabled(true);
        account.setPassword("sw0rdf1sh");
        account.setAddress("alice@wonderland.lit");
        account.setRosterVersion("");
        account.setHost("wonderland.lit");
        account.setPort(5222);

        // Insert account into database
        TestObserver<Account> testObserver = new TestObserver<>();
        accountRepository.upsertAccount(account)
                .subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertNoTimeout();
        testObserver.assertValue(account);

        // delete account from database
        testObserver = new TestObserver<>();
        accountRepository.deleteAccount(account)
                .subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertNoTimeout();

        // assert no account in database
        testObserver = new TestObserver<>();
        accountRepository.getAccount(account.getId()).subscribe(testObserver);

        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertNoValues();
    }


}
