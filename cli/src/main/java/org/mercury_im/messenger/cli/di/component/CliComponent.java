package org.mercury_im.messenger.cli.di.component;

import org.mercury_im.messenger.cli.MercuryCli;
import org.mercury_im.messenger.cli.di.module.CliDatabaseModule;
import org.mercury_im.messenger.cli.di.module.CliSchedulersModule;
import org.mercury_im.messenger.cli.di.module.X509WorkaroundConnectionFactoryModule;
import org.mercury_im.messenger.core.di.module.OpenPgpModule;
import org.mercury_im.messenger.core.di.module.RxMercuryMessageStoreFactoryModule;
import org.mercury_im.messenger.core.di.module.RxMercuryRosterStoreFactoryModule;
import org.mercury_im.messenger.core.di.module.StanzaIdSourceFactoryModule;
import org.mercury_im.messenger.core.di.module.ViewModelModule;
import org.mercury_im.messenger.data.di.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                CliDatabaseModule.class,
                CliSchedulersModule.class,
                RepositoryModule.class,
                ViewModelModule.class,
                X509WorkaroundConnectionFactoryModule.class,
                RxMercuryMessageStoreFactoryModule.class,
                RxMercuryRosterStoreFactoryModule.class,
                OpenPgpModule.class,
                StanzaIdSourceFactoryModule.class
        })
public interface CliComponent {

        void inject(MercuryCli mercuryCli);
}
