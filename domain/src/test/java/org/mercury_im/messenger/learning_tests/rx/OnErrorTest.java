package org.mercury_im.messenger.learning_tests.rx;

import org.junit.jupiter.api.Test;

import io.reactivex.Observable;
import io.reactivex.Single;

public class OnErrorTest {

    @Test
    public void observableErrorTest() throws InterruptedException {
        Observable<String> errorObservable = Observable.error(new AssertionError());
        Observable<String> fallbackObservable = Observable.just("String");

        errorObservable
                .doOnError(e -> System.out.println("Caught here."))
                .onErrorResumeNext(fallbackObservable)
                .flatMap(s -> Observable.just(s)
                                .doOnError(e -> System.out.println("Or here?")))
                .doOnError(e -> System.out.println("Or even here?"))
                .subscribe(s -> System.out.println("Result: " + s),
                        e -> System.out.println("Finally here."));
        Thread.sleep(100);
    }

    @Test
    public void singleErrorTest() throws InterruptedException {
        Single<String> errorSingle = Single.error(new AssertionError());
        Single<String> successSingle = Single.just("Item");

        successSingle
                .map(s -> "Map: " + s)
                .onErrorResumeNext(Single.just("cancel"))
                .subscribe(s -> System.out.println(s),
                        e -> System.out.println("Error"));
        Thread.sleep(100);
    }
}
