package org.mercury_im.messenger.core.connection.message;

import org.mercury_im.messenger.core.connection.MercuryConnectionManager;
import org.mercury_im.messenger.core.data.repository.MessageRepository;
import org.mercury_im.messenger.entity.chat.Chat;

import java.util.UUID;

import io.reactivex.Single;

public class OxMessageConsignor extends AbstractMessageConsignor {

    public OxMessageConsignor(MercuryConnectionManager connectionManager, MessageRepository messageRepository, Chat chat) {
        this(new MessageComposer(), connectionManager, messageRepository, chat);
    }

    public OxMessageConsignor(MessageComposer composer, MercuryConnectionManager connectionManager, MessageRepository messageRepository, Chat chat) {
        super(composer, connectionManager, messageRepository, chat);
    }

    @Override
    public Single<UUID> sendTextMessage(Chat chat, String body) {
        return null;
    }
}
