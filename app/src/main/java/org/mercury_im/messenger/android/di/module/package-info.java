/**
 * Modules define the methods that return the objects you’ll need to inject in your classes.
 * If a method is dependent on passing in a parameter, then you need to define an additional
 * method that will return the object to be passed into the original method. The module will
 * know which method to run in order to obtain the object being injected.
 *
 * @see <a href="https://android.jlelse.eu/the-simplest-dagger2-dependency-injection-sample-80a0eb60e33b">The Simples Dagger2 Dependency Injection Sample App</a>
 */
package org.mercury_im.messenger.android.di.module;
