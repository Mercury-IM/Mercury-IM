package org.mercury_im.messenger.data.di.module;

import org.mercury_im.messenger.data.model.Models;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;

import java.io.PrintWriter;
import java.sql.SQLException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.cache.WeakEntityCache;
import io.requery.meta.EntityModel;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import io.requery.sql.SchemaModifier;
import io.requery.sql.TableCreationMode;

@Module
public class SqliteTestDatabaseModule {

    @Provides
    @Singleton
    public static ReactiveEntityStore<Persistable> provideInMemoryEntityStore() {
        EntityModel model = Models.DEFAULT;

        SQLiteDataSource dataSource = new SQLiteDataSource();
        try {
            dataSource.setLogWriter(new PrintWriter(System.out));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        dataSource.setUrl("jdbc:sqlite:/tmp/mercury_testing.db");
        dataSource.setDatabaseName("testing");
        SQLiteConfig config = new SQLiteConfig();
        config.setDateClass("TEXT");
        dataSource.setConfig(config);
        // Turn on foreign keys support.
        // NOTE: Do it after setConfig, or setConfig will overwrite this setting
        dataSource.setEnforceForeignKeys(true);

        SchemaModifier modifier = new SchemaModifier(dataSource, model);
        modifier.createTables(TableCreationMode.DROP_CREATE);

        Configuration configuration = new ConfigurationBuilder(dataSource, model)
                .setEntityCache(new WeakEntityCache())
                .useDefaultLogging()
                .build();

        EntityDataStore<Persistable> dataStore = new EntityDataStore<>(configuration);
        return ReactiveSupport.toReactiveStore(dataStore);
    }
}
