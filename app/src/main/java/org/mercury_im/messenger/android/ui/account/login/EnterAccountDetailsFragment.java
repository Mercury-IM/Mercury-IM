package org.mercury_im.messenger.android.ui.account.login;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.di.component.AppComponent;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterAccountDetailsFragment extends Fragment {

    @BindView(R.id.username_layout)
    TextInputLayout usernameLayout;

    @BindView(R.id.username)
    TextInputEditText username;

    @BindView(R.id.password_layout)
    TextInputLayout passwordLayout;

    @BindView(R.id.password)
    TextInputEditText password;

    @BindView(R.id.btn_login)
    Button loginButton;

    @BindView(R.id.btn_cancel)
    Button cancelButton;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject
    AndroidLoginViewModel viewModel;

    public EnterAccountDetailsFragment(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enter_account_credentials, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new AndroidLoginViewModel(MercuryImApplication.getApplication());

        loginButton.setOnClickListener(v -> viewModel.onLoginButtonClicked());

        cancelButton.setOnClickListener(v -> getActivity().finish());

        viewModel.getLoginUsernameError().observe(this, error -> usernameLayout.setError(error));
        viewModel.getLoginPasswordError().observe(this, error -> passwordLayout.setError(error));
        viewModel.isLoginButtonEnabled().observe(this, loginButton::setEnabled);
        viewModel.isDisplayProgressBar().observe(this, visible ->
                progressBar.setVisibility(visible ? View.VISIBLE : View.GONE));

        viewModel.isLoginFinished().observe(this, optAccount ->
                ((AddAccountActivity) getActivity()).loginFinished(optAccount));

        username.addTextChangedListener(viewModel.getUsernameTextChangedListener());
        password.addTextChangedListener(viewModel.getPasswordTextChangedListener());

        username.setOnEditorActionListener(focusPasswordFieldOnEnterPressed);
        password.setOnEditorActionListener(loginOnEnterPressed);
    }

    private final TextView.OnEditorActionListener focusPasswordFieldOnEnterPressed = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                password.requestFocus();
                return true;
            }
            return false;
        }
    };

    private final TextView.OnEditorActionListener loginOnEnterPressed = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                viewModel.onLoginButtonClicked();
                return true;
            }
            return false;
        }
    };

    public static EnterAccountDetailsFragment newInstance(AppComponent appComponent) {
        return new EnterAccountDetailsFragment(appComponent);
    }
}
