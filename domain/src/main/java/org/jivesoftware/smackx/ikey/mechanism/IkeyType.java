package org.jivesoftware.smackx.ikey.mechanism;

public enum IkeyType {
    OX,
    X509
}
