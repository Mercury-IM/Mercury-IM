package org.jivesoftware.smackx.signed.xep0285;

import org.jivesoftware.smack.parsing.SmackParsingException;
import org.jivesoftware.smack.util.ParserUtils;
import org.jivesoftware.smack.xml.XmlPullParserException;
import org.jivesoftware.smackx.signed.xep0285.element.PlainElement;
import org.jivesoftware.smackx.signed.xep0285.provider.PlainElementProvider;
import org.jivesoftware.smackx.util.MercurySmackTestSuite;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlainElementTest extends MercurySmackTestSuite {

    @Test
    public void test() throws XmlPullParserException, IOException, SmackParsingException {
        String plain = "<message xmlns=\"jabber:client\" " +
                "from=\"juliet@capulet.net/balcony\" " +
                "to=\"romeo@montegue.net\" " +
                "type=\"chat\">" +
                "<thread>c6373824-a307-40dd-8fe0-bad6e7299ad0</thread>" +
                "<body>Wherefore art thou, Romeo?</body>" +
                "</message>";
        String example2 = "<plain xmlns=\"urn:xmpp:signed:0\" " +
                "timestamp=\"2010-06-29T02:15:21.012+00:00\">" +
                "PG1lc3NhZ2UgeG1sbnM9ImphYmJlcjpjbGllbnQiIGZyb209Imp1bGlldEBjYXB" +
                "1bGV0Lm5ldC9iYWxjb255IiB0bz0icm9tZW9AbW9udGVndWUubmV0IiB0eXBlPS" +
                "JjaGF0Ij48dGhyZWFkPmM2MzczODI0LWEzMDctNDBkZC04ZmUwLWJhZDZlNzI5O" +
                "WFkMDwvdGhyZWFkPjxib2R5PldoZXJlZm9yZSBhcnQgdGhvdSwgUm9tZW8/PC9i" +
                "b2R5PjwvbWVzc2FnZT4=" +
                "</plain>";

        PlainElement parsed = new PlainElementProvider()
                .parse(getParser(example2));
        PlainElement fromPlain = PlainElement.fromUtf8String(plain, ParserUtils.getDateFromXep82String("2010-06-29T02:15:21.012+00:00"));

        assertEquals(parsed, fromPlain);
        assertEquals(plain, parsed.getUtf8Content());
    }
}
