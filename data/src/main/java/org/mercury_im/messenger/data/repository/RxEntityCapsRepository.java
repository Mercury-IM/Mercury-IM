package org.mercury_im.messenger.data.repository;

import org.mercury_im.messenger.core.data.repository.EntityCapsRepository;
import org.mercury_im.messenger.data.mapping.EntityCapsMapping;
import org.mercury_im.messenger.data.model.EntityCapsModel;
import org.mercury_im.messenger.entity.caps.EntityCapsRecord;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class RxEntityCapsRepository extends RequeryRepository implements EntityCapsRepository {

    private final EntityCapsMapping entityCapsMapping;

    @Inject
    public RxEntityCapsRepository(
            ReactiveEntityStore<Persistable> data,
            EntityCapsMapping mapping) {
        super(data);
        this.entityCapsMapping = mapping;
    }

    @Override
    public Observable<Map<String, EntityCapsRecord>> observeAllEntityCapsRecords() {
        return data().select(EntityCapsModel.class).get()
                .observableResult()
                .map(result -> result.toMap(EntityCapsModel.NODE_VER, new ConcurrentHashMap<>()))
                .map(this::mapModelsToEntities);
    }

    private Map<String, EntityCapsRecord> mapModelsToEntities(Map<String, EntityCapsModel> models) {
        Map<String, EntityCapsRecord> entities = new ConcurrentHashMap<>();
        for (String key : models.keySet()) {
            entities.put(key, entityCapsMapping.toEntity(models.get(key)));
        }
        return entities;
    }

    private Map<String, EntityCapsModel> mapEntitiesToModels(Map<String, EntityCapsRecord> entities) {
        Map<String, EntityCapsModel> models = new ConcurrentHashMap<>();
        for (String key : entities.keySet()) {
            models.put(key, entityCapsMapping.toModel(entities.get(key)));
        }
        return models;
    }

    @Override
    public Observable<EntityCapsRecord> observeEntityCapsRecords() {
        return data().select(EntityCapsModel.class)
                .get().observableResult()
                .flatMap(ReactiveResult::observable)
                .map(entityCapsMapping::toEntity);
    }

    @Override
    public Maybe<EntityCapsRecord> maybeGetEntityCapsRecord(String nodeVer) {
        return data().select(EntityCapsModel.class)
                .where(EntityCapsModel.NODE_VER.eq(nodeVer))
                .get().maybe()
                .map(entityCapsMapping::toEntity);
    }

    @Override
    public Completable insertEntityCapsRecord(EntityCapsRecord entityCapsRecord) {
        return data().upsert(entityCapsMapping.toModel(entityCapsRecord))
                .ignoreElement();
    }

    @Override
    public Single<Integer> deleteAllEntityCapsRecords() {
        return data().delete().from(EntityCapsModel.class)
                .get().single();
    }
}
