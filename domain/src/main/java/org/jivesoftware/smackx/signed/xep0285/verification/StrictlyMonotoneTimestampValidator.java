package org.jivesoftware.smackx.signed.xep0285.verification;

import org.jivesoftware.smackx.signed.xep0285.element.PlainElement;

import java.util.Date;

/**
 * So boring...
 */
public class StrictlyMonotoneTimestampValidator implements TimestampValidator {

    private Date lastTimestamp;

    public StrictlyMonotoneTimestampValidator(Date lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
    }

    @Override
    public boolean isValid(PlainElement plainElement) {
        if (plainElement.getTimestamp() == null) {
            return false;
        }
        return lastTimestamp == null || plainElement.getTimestamp().after(lastTimestamp);
    }

    public void set(PlainElement plainElement) {
        this.lastTimestamp = plainElement.getTimestamp();
    }
}
