package org.mercury_im.messenger.data.model;

import org.mercury_im.messenger.data.converter.SubscriptionDirectionConverter;
import org.mercury_im.messenger.entity.contact.SubscriptionDirection;

import java.util.List;
import java.util.UUID;

import io.requery.CascadeAction;
import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Index;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;
import io.requery.ReferentialAction;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Entity
@Table(name = "contacts", uniqueIndexes = "unique_address")
public abstract class AbstractPeerModel implements Persistable {

    @Key
    @Convert(UUIDConverter.class)
    UUID id;

    @Index("unique_address")
    @ManyToOne(cascade = CascadeAction.NONE)
    @ForeignKey(referencedColumn = "id")
    AccountModel account;

    @Index("unique_address")
    @Column(nullable = false)
    String address;

    @Column
    String name;

    @Convert(SubscriptionDirectionConverter.class)
    SubscriptionDirection subscriptionDirection;

    @Column
    List<String> groupNames;

    boolean subscriptionPending;

    boolean subscriptionPreApproved;

    @Override
    public String toString() {
        return "Peer[" + id + ", " +
                name + ", " +
                address + ", " +
                account + "]";
    }
}
