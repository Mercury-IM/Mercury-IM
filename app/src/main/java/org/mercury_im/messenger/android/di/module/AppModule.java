package org.mercury_im.messenger.android.di.module;

import android.app.Application;

import org.mercury_im.messenger.android.MercuryImApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private MercuryImApplication mApplication;

    public AppModule(MercuryImApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }
}
