package org.jivesoftware.smackx.ikey.element;

import org.jivesoftware.smack.packet.NamedElement;
import org.jivesoftware.smack.packet.XmlEnvironment;
import org.jivesoftware.smack.util.EqualsUtil;
import org.jivesoftware.smack.util.HashCode;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smack.util.stringencoder.Base64;

public class ProofElement implements NamedElement {

    public static final String ELEMENT = "proof";

    private final String base64Sig;

    public ProofElement(String base64Sig) {
        this.base64Sig = base64Sig;
    }

    public ProofElement(byte[] plainBytes) {
        this(Base64.encodeToString(plainBytes));
    }

    public String getBase64Signature() {
        return base64Sig;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public XmlStringBuilder toXML(XmlEnvironment xmlEnvironment) {
        return new XmlStringBuilder(this).rightAngleBracket()
                .append(getBase64Signature())
                .closeElement(this);
    }

    @Override
    public int hashCode() {
        return HashCode.builder()
                .append(getElementName())
                .append(getBase64Signature()).build();
    }

    @Override
    public boolean equals(Object other) {
        return EqualsUtil.equals(this, other, (e, o) -> e
                .append(getElementName(), o.getElementName())
                .append(getBase64Signature(), o.getBase64Signature()));
    }
}
