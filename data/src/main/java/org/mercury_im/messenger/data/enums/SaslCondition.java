package org.mercury_im.messenger.data.enums;

public enum  SaslCondition {

    // Success

    /**
     * The SASL handshake is considered successful.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-process-neg-success">rfc6120 $6.4.6: SASL Success</a>
     */
    success,

    // Failure

    /**
     * The receiving entity acknowledges that the authentication handshake has been aborted by the
     * initiating entity; sent in reply to the <pre><abort/></pre> element.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-aborted">rfc6120 §6.5.1: aborted</a>
     */
    aborted,

    /**
     * The account of the initiating entity has been temporarily disabled; sent in reply to an
     * <pre><auth/></pre> element (with or without initial response data) or a
     * <pre><response/></pre> element.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-account-disabled">rfc6120 §6.5.2: account-disabled</a>
     */
    account_disabled,

    /**
     * The authentication failed because the initiating entity provided credentials that have
     * expired; sent in reply to a <pre><response/></pre> element or an <pre><auth/></pre> element
     * with initial response data.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-credentials-expired">rfc6120 §6.5.3: credentials-expired</a>
     */
    credentials_expired,

    /**
     * The mechanism requested by the initiating entity cannot be used unless the confidentiality
     * and integrity of the underlying stream are protected (typically via TLS);
     * sent in reply to an <pre><auth/></pre> element (with or without initial response data).
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-encryption-required">rfc6120 §6.5.4: encryption-required</a>
     */
    encryption_required,

    /**
     * The data provided by the initiating entity could not be processed because the base 64
     * encoding is incorrect (e.g., because the encoding does not adhere to the definition in
     * Section 4 of [BASE64]); sent in reply to a <pre><response/></pre> element or an
     * <pre><auth/></pre> element with initial response data.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-incorrect-encoding">rfc6120 §6.5.5: incorrect-encoding</a>
     */
    incorrect_encoding,

    /**
     * The authzid provided by the initiating entity is invalidUsername, either because it is incorrectly
     * formatted or because the initiating entity does not have permissions to authorize that ID;
     * sent in reply to a <pre><response/></pre> element or an <pre><auth/></pre> element with
     * initial response data.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-invalid-authzid">rfc6120 §6.5.6: invalidUsername-authzid</a>
     */
    invalid_authzid,

    /**
     * The initiating entity did not specify a mechanism, or requested a mechanism that is not
     * supported by the receiving entity; sent in reply to an <pre><auth/></pre> element.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-invalid-mechanism">rfc6120 §6.5.7: invalidUsername-mechanism</a>
     */
    invalid_mechanism,

    /**
     * The request is malformed (e.g., the <pre><auth/></pre> element includes initial response
     * data but the mechanism does not allow that, or the data sent violates the syntax for the
     * specified SASL mechanism);
     * sent in reply to an <pre><abort/></pre>, <pre><auth/></pre>, <pre><challenge/></pre>,
     * or <pre><response/></pre> element.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-malformed-request">rfc6120 §6.5.8: malformed-request</a>
     */
    malformed_request,

    /**
     * The mechanism requested by the initiating entity is weaker than server policy permits for
     * that initiating entity; sent in reply to an <pre><auth/></pre> element (with or without
     * initial response data).
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-mechanism-too-weak">rfc6120 §6.5.9: mechanism-too-weak</a>
     */
    mechanism_too_weak,

    /**
     * The authentication failed because the initiating entity did not provide proper credentials,
     * or because some generic authentication failure has occurred but the receiving entity does
     * not wish to disclose specific information about the cause of the failure;
     * sent in reply to a <pre><response/></pre> element or an <pre><auth/></pre> element with
     * initial response data.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-not-authorized">rfc6120 §6.5.10: not-authorized</a>
     */
    not_authorized,

    /**
     * The authentication failed because of a temporary error condition within the receiving entity,
     * and it is advisable for the initiating entity to try again later;
     * sent in reply to an <pre><auth/></pre> element or a <pre><response/></pre> element.
     *
     * @see <a href="https://xmpp.org/rfcs/rfc6120.html#sasl-errors-temporary-auth-failure">rfc6120 §6.5.11: temporary-auth-failure</a>
     */
    temporary_auth_failure
}
