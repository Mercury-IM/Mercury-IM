package org.mercury_im.messenger.core.connection.state;

public enum ConnectivityState {
    disconnected,
    connecting,
    connected,
    authenticated,
    disconnectedOnError
}
