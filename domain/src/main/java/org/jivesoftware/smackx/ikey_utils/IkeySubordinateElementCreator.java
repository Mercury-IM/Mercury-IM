package org.jivesoftware.smackx.ikey_utils;

import org.jivesoftware.smackx.ikey.element.SubordinateElement;
import org.jivesoftware.smackx.omemo.internal.OmemoDevice;
import org.jivesoftware.smackx.omemo.trust.OmemoFingerprint;
import org.jivesoftware.smackx.ox.element.OpenPgpElement;
import org.jivesoftware.smackx.ox.util.OpenPgpPubSubUtil;
import org.jivesoftware.smackx.pubsub.PubSubUri;
import org.jxmpp.jid.BareJid;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.net.URI;
import java.net.URISyntaxException;

public class IkeySubordinateElementCreator {

    public static SubordinateElement createOxSubordinateElement(BareJid pubSubService, OpenPgpV4Fingerprint fingerprint, String itemId)
            throws URISyntaxException {
        String node = OpenPgpPubSubUtil.PEP_NODE_PUBLIC_KEY(fingerprint);
        PubSubUri pubSubUri = new PubSubUri(pubSubService, node, itemId, null);
        URI uri = new URI(pubSubUri.toString());
        return new SubordinateElement(OpenPgpElement.NAMESPACE, uri, fingerprint.toString());
    }

    public static SubordinateElement createOmemoSubordinateElement(BareJid pubSubService, OmemoDevice device, OmemoFingerprint fingerprint)
        throws URISyntaxException {
        PubSubUri pubSubUri = new PubSubUri(pubSubService, "urn:xmpp:omemo:1:bundles", Integer.toString(device.getDeviceId()), null);
        URI uri = new URI(pubSubUri.toString());
        return new SubordinateElement("urn:xmpp:omemo:1", uri, fingerprint.toString());
    }

    public static SubordinateElement createSiacsOmemoSubordinateElement(BareJid pubSubService, OmemoDevice device, OmemoFingerprint fingerprint)
            throws URISyntaxException {
        PubSubUri pubSubUri = new PubSubUri(pubSubService, device.getBundleNodeName(), null, null);
        URI uri = new URI(pubSubUri.toString());
        return new SubordinateElement("eu.siacs.conversations.axolotl", uri, fingerprint.toString());
    }
}
