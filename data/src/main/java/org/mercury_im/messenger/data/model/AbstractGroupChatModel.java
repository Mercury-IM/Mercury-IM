package org.mercury_im.messenger.data.model;

import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.ManyToOne;
import io.requery.Persistable;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Entity
@Table(name = "groupchats")
public abstract class AbstractGroupChatModel implements Persistable {

    @Key
    @Convert(UUIDConverter.class)
    UUID id;

    @Column(nullable = false)
    @ManyToOne
    AccountModel account;

    @Column(nullable = false)
    String address;

    @Column
    String name;

    @Column
    boolean auto_join;
}
