package org.mercury_im.messenger.android.ui.base;

import android.content.Intent;
import android.os.Bundle;

public interface MercuryActivity {

    default Bundle requiredExtras(Bundle savedInstanceState, Intent intent) {
        if (savedInstanceState == null) {
            savedInstanceState = intent.getExtras();
        }
        if (savedInstanceState == null) {
            throw new IllegalArgumentException("Missing bundle!");
        }
        return savedInstanceState;
    }
}
