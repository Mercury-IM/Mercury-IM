package org.mercury_im.messenger.data.repository.dao;

import org.mercury_im.messenger.data.model.AccountModel;

import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class AccountDao extends RequeryDao {

    @Inject
    public AccountDao(ReactiveEntityStore<Persistable> data) {
        super(data);
    }

    public Single<AccountModel> insert(AccountModel account) {
        return data().insert(account);
    }

    public ReactiveResult<AccountModel> get(UUID accountId) {
        return data().select(AccountModel.class)
                .where(AccountModel.ID.eq(accountId))
                .get();
    }

    public ReactiveResult<AccountModel> get(String address) {
        return data().select(AccountModel.class)
                .where(AccountModel.ADDRESS.eq(address))
                .get();
    }

    public ReactiveResult<AccountModel> getAll() {
        return data().select(AccountModel.class)
                .get();
    }

    public Single<Integer> delete(UUID accountId) {
        return data().delete(AccountModel.class)
                .where(AccountModel.ID.eq(accountId))
                .get().single();
    }
}
