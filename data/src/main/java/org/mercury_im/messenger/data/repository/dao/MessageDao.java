package org.mercury_im.messenger.data.repository.dao;

import org.mercury_im.messenger.data.model.DirectChatModel;
import org.mercury_im.messenger.data.model.GroupChatModel;
import org.mercury_im.messenger.data.model.MessageModel;

import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class MessageDao extends RequeryDao {

    private final DirectChatDao directChatDao;
    private final GroupChatDao groupChatDao;

    @Inject
    public MessageDao(ReactiveEntityStore<Persistable> data) {
        super(data);
        this.directChatDao = new DirectChatDao(data);
        this.groupChatDao = new GroupChatDao(data);
    }

    public Single<MessageModel> insert(MessageModel message) {
        return data().insert(message);
    }

    public ReactiveResult<MessageModel> get(UUID messageId) {
        return data().select(MessageModel.class)
                .where(MessageModel.ID.eq(messageId))
                .get();
    }
}
