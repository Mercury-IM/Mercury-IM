package org.mercury_im.messenger.data.converter;

import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.impl.JidCreate;

import javax.annotation.Nullable;

import io.requery.Converter;

public class EntityJidConverter implements Converter<EntityJid, String> {
    @Override
    public Class<EntityJid> getMappedType() {
        return EntityJid.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable
    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(EntityJid value) {
        if (value == null) return null;
        return value.toString();
    }

    @Override
    public EntityJid convertToMapped(Class<? extends EntityJid> type, @Nullable String value) {
        if (value == null) {
            return null;
        }
        return JidCreate.entityFromOrThrowUnchecked(value);
    }
}
