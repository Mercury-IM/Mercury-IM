package org.mercury_im.messenger.data.converter;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;

import javax.annotation.Nullable;

import io.requery.Converter;

public class EntityBareJidConverter implements Converter<EntityBareJid, String> {

    @Override
    public Class<EntityBareJid> getMappedType() {
        return EntityBareJid.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable
    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(EntityBareJid value) {
        return value == null ? null : value.asEntityBareJidString();
    }

    @Override
    public EntityBareJid convertToMapped(Class<? extends EntityBareJid> type, @Nullable String value) {
        return value == null ? null : JidCreate.entityBareFromOrThrowUnchecked(value);
    }
}
