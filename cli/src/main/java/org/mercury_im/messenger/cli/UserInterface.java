package org.mercury_im.messenger.cli;

public interface UserInterface {

    String readText();

    String readPassword();
}
