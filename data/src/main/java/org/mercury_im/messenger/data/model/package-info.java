/**
 * The model package contains requery model definitions.
 * All of those classes are abstract, since the requery framework generates concrete implementations
 * during compilation. Those files can later be found in
 * <pre>build/generated/sources/annotationProcessor/java/main/...</pre>.
 *
 * The structure of the model classes closely mimics the structure of their entity pendants
 * declared in the <pre>entity</pre> module.
 *
 * @see <a href="https://github.com/requery/requery/wiki/Defining-Entities">requery wiki on model definitions</a>
 */
package org.mercury_im.messenger.data.model;
