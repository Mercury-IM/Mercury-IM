# Entity

This module contains entity definitions for MercuryIM.
According to Robert C. Martins "Clean Architecture", entities form the inner most layer of the
program.