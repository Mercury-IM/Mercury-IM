package org.mercury_im.messenger.entity.contact;

public enum  SubscriptionDirection {
    none,
    to,
    from,
    both,
    remove
}
