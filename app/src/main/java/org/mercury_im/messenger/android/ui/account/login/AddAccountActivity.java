package org.mercury_im.messenger.android.ui.account.login;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import org.mercury_im.messenger.R;
import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.di.component.AppComponent;
import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.entity.Account;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;

public class AddAccountActivity extends AppCompatActivity implements IkeySetupNavigator {

    @BindView(R.id.viewpager)
    ViewPager2 viewPager;

    private SetupPagerAdapter pagerAdapter;

    private AppComponent appComponent;
    private AndroidIkeySetupViewModel ikeyViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_viewpager2);
        ButterKnife.bind(this);
        appComponent = MercuryImApplication.getApplication().getAppComponent();

        pagerAdapter = new SetupPagerAdapter(this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setUserInputEnabled(false); // disable swiping

        ikeyViewModel = new ViewModelProvider(this).get(AndroidIkeySetupViewModel.class); // shared between fragments
    }

    public void loginFinished(Optional<Account> optionalAccount) {
        if (optionalAccount.isPresent()) {
            pagerAdapter.getFragments().put(1, IkeySetupFragment.newInstance(optionalAccount.getItem().getId()));
            pagerAdapter.notifyDataSetChanged();
            viewPager.setCurrentItem(1);
        }
    }

    @Override
    public void setupIkey(UUID accountId) {
        int nextPos = pagerAdapter.getItemCount();
        pagerAdapter.getFragments().put(nextPos, IkeyBackupRestoreOrSkipFragment.newInstance(accountId));
        pagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(nextPos);
    }

    @Override
    public void restoreSuccessful(UUID accountId) {
        int nextPos = pagerAdapter.getItemCount();
        pagerAdapter.getFragments().put(nextPos, IkeyKeyInfoFragment.newInstance(accountId));
        pagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(nextPos);
    }

    @Override
    public void generateIkeyBackup(UUID accountId) {
        int nextPos = pagerAdapter.getItemCount();
        pagerAdapter.getFragments().put(nextPos, IkeyBackupCreationFragment.newInstance(accountId));
        pagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(nextPos);
    }

    @Override
    public void displayInfo(UUID accountId) {
        int nextPos = pagerAdapter.getItemCount();
        pagerAdapter.getFragments().put(nextPos, IkeyKeyInfoFragment.newInstance(accountId));
        pagerAdapter.notifyDataSetChanged();
        viewPager.setCurrentItem(nextPos);
    }

    private class SetupPagerAdapter extends FragmentStateAdapter {

        @Getter
        private final Map<Integer, Fragment> fragments = new LinkedHashMap<>();

        SetupPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
            super(fragmentActivity);

            fragments.put(0, EnterAccountDetailsFragment.newInstance(appComponent));
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            return fragments.get(position);
        }

        @Override
        public int getItemCount() {
            return fragments.size();
        }
    }

}
