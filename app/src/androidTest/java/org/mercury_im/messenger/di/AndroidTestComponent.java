package org.mercury_im.messenger.di;

import org.mercury_im.messenger.RequeryDatabaseTest;
import org.mercury_im.messenger.data.di.MappingModule;
import org.mercury_im.messenger.data.di.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        TestDatabaseModule.class,
        RepositoryModule.class,
        MappingModule.class
})
public interface AndroidTestComponent {

    void inject(RequeryDatabaseTest test);

}
