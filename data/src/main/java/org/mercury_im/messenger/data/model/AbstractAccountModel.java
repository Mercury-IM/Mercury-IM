package org.mercury_im.messenger.data.model;

import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Table(name = "accounts")
@Entity
public abstract class AbstractAccountModel implements Persistable {

    @Key
    @Convert(UUIDConverter.class)
    UUID id;

    @Column(nullable = false)
    String address;

    @Column(nullable = false)
    String password;

    @Column
    String host;

    @Column
    int port;

    @Column
    boolean enabled;

    @Column(nullable = false, value = "")
    String rosterVersion;

    @Override
    public String toString() {
        return "Account[" + id + ", " +
                address + ", " +
                (enabled ? "enabled" : "disabled") + "]";
    }
}
