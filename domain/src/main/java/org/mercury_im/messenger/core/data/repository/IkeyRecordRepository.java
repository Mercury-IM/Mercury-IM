package org.mercury_im.messenger.core.data.repository;

import org.jivesoftware.smackx.ikey.element.IkeyElement;
import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.entity.Account;

import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;

public interface IkeyRecordRepository {

    default Observable<IkeyElement> loadRecord(Account account, EntityBareJid jid) {
        return loadRecord(account.getId(), jid);
    }

    Observable<IkeyElement> loadRecord(UUID accountId, EntityBareJid jid);

    default Completable storeRecord(Account account, EntityBareJid jid, IkeyElement record) {
        return storeRecord(account.getId(), jid, record);
    }

    Completable storeRecord(UUID accountId, EntityBareJid jid, IkeyElement record);

}
