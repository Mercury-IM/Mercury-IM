package org.jivesoftware.smackx.ikey_utils;

import org.jivesoftware.smackx.ikey.element.SubordinateElement;
import org.jivesoftware.smackx.omemo.internal.OmemoDevice;
import org.jivesoftware.smackx.omemo.trust.OmemoFingerprint;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IkeySubordinateElementCreatorTest {

    @Test
    public void testOxSubordinateElementCreation() throws URISyntaxException {
        String xml = "<sub uri='xmpp:hamlet@denmark.lit?;node=urn:xmpp:openpgp:0:public-keys:1357B01865B2503C18453D208CAC2A9678548E35;item=2020-01-21T10:46:21Z' " +
                "fpr='1357B01865B2503C18453D208CAC2A9678548E35'/>";

        BareJid pubSubService = JidCreate.bareFromOrThrowUnchecked("hamlet@denmark.lit");
        OpenPgpV4Fingerprint fingerprint = new OpenPgpV4Fingerprint("1357B01865B2503C18453D208CAC2A9678548E35");
        String itemId = "2020-01-21T10:46:21Z";

        SubordinateElement element = IkeySubordinateElementCreator.createOxSubordinateElement(pubSubService, fingerprint, itemId);

        assertEquals(xml, element.toXML().toString());
    }

    @Test
    public void testOmemoSubordinateElementCreation() throws URISyntaxException {
        String xml = "<sub uri='xmpp:hamlet@denmark.lit?;node=urn:xmpp:omemo:1:bundles;item=31415' " +
                "fpr='e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f'/>";

        BareJid pubSubService = JidCreate.bareFromOrThrowUnchecked("hamlet@denmark.lit");
        OmemoFingerprint fingerprint = new OmemoFingerprint("e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f");

        SubordinateElement element = IkeySubordinateElementCreator.createOmemoSubordinateElement(pubSubService, new OmemoDevice(pubSubService, 31415), fingerprint);

        assertEquals(xml, element.toXML().toString());
    }

    @Test
    public void testSiacsOmemoSubordinateElementCreation() throws URISyntaxException {
        String xml = "<sub uri='xmpp:hamlet@denmark.lit?;node=eu.siacs.conversations.axolotl.bundles:31415' " +
                "fpr='e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f'/>";

        BareJid pubSubService = JidCreate.bareFromOrThrowUnchecked("hamlet@denmark.lit");
        OmemoFingerprint fingerprint = new OmemoFingerprint("e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f");

        SubordinateElement element = IkeySubordinateElementCreator.createSiacsOmemoSubordinateElement(pubSubService, new OmemoDevice(pubSubService, 31415), fingerprint);

        assertEquals(xml, element.toXML().toString());
    }
}
