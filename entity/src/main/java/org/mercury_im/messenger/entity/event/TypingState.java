package org.mercury_im.messenger.entity.event;

public enum TypingState {
    /**
     * The peer is typing.
     */
    typing,

    /**
     * The peer was typing and is now pausing for a short amount of time.
     */
    pause,

    /**
     *  The peer is deleting from their input field.
     */
    deleting,

    /**
     * The peer stopped typing completely.
     * TODO: Makes sense?
     */
    stop
}
