package org.jivesoftware.smackx.ikey.util;

public class IkeyConstants {

    public static final String NAMESPACE = "urn:xmpp:ikey:0";
    public static final String SUBORDINATES_NODE = NAMESPACE + ":subordinates";
    public static final String SUPERORDINATE_NODE = NAMESPACE + ":superordinate";
}
