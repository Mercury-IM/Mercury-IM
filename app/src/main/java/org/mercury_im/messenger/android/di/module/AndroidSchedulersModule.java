package org.mercury_im.messenger.android.di.module;

import org.mercury_im.messenger.core.SchedulersFacade;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class AndroidSchedulersModule {

    @Provides
    @Named(value = SchedulersFacade.SCHEDULER_IO)
    @Singleton
    static Scheduler provideDatabaseThread() {
        return Schedulers.io();
    }

    @Provides
    @Named(value = SchedulersFacade.SCHEDULER_UI)
    @Singleton
    static Scheduler provideUIThread() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Named(value = SchedulersFacade.SCHEDULER_NEW_THREAD)
    static Scheduler provideNewThread() {
        return Schedulers.newThread();
    }
}
