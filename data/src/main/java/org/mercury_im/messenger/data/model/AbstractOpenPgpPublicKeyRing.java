package org.mercury_im.messenger.data.model;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.data.converter.EntityBareJidConverter;
import org.mercury_im.messenger.entity.Account;

import java.util.UUID;

import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Key;
import io.requery.ReferentialAction;
import io.requery.Table;

@Entity
@Table(name = "ox_public_keys")
public class AbstractOpenPgpPublicKeyRing {

    @Key
    @Column(name = "account_id", nullable = false)
    @ForeignKey(references = AbstractAccountModel.class, delete = ReferentialAction.CASCADE)
    UUID accountId;

    @Key
    @Column(name = "owner", nullable = false)
    @Convert(EntityBareJidConverter.class)
    EntityBareJid owner;

    @Column(name = "data", nullable = false)
    byte[] bytes;
}
