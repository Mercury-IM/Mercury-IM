package org.mercury_im.messenger.entity.event;

import org.mercury_im.messenger.entity.contact.Peer;

import java.util.Map;

import lombok.Data;
import lombok.Value;

/**
 * Event of someone typing in a chat.
 */
@Value
public class TypingEvent {
    Map<Peer, TypingState> typingStates;
}
