package org.mercury_im.messenger.entity.message;

public enum MessageDirection {
    incoming,
    outgoing
}
