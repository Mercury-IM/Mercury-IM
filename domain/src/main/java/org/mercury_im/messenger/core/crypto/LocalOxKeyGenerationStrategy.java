package org.mercury_im.messenger.core.crypto;

public interface LocalOxKeyGenerationStrategy {

    boolean promptForBackupRestoreIfNoLocalKeyPresent();
}
