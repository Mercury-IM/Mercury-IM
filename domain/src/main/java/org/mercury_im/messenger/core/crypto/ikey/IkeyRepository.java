package org.mercury_im.messenger.core.crypto.ikey;

import org.mercury_im.messenger.core.data.repository.IkeyKeyRepository;
import org.mercury_im.messenger.core.data.repository.IkeyRecordRepository;

public interface IkeyRepository extends IkeyKeyRepository, IkeyRecordRepository {

}
