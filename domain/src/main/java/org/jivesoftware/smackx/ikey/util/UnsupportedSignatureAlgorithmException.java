package org.jivesoftware.smackx.ikey.util;

import org.jivesoftware.smack.util.StringUtils;

public class UnsupportedSignatureAlgorithmException extends Exception {

    private static final long serialVersionUID = 1L;
    private final String algorithmName;

    public UnsupportedSignatureAlgorithmException(String algorithmName) {
        this.algorithmName = StringUtils.requireNotNullNorEmpty(algorithmName, "Algorithm name MUST NOT be null NOR empty.");
    }

    public String getAlgorithmName() {
        return algorithmName;
    }
}
