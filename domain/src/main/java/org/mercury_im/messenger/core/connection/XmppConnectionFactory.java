package org.mercury_im.messenger.core.connection;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.mercury_im.messenger.entity.Account;

public interface XmppConnectionFactory {

    AbstractXMPPConnection createConnection(Account account);

}
