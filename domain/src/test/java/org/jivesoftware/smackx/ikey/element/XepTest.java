package org.jivesoftware.smackx.ikey.element;

import org.bouncycastle.openpgp.PGPException;
import org.jivesoftware.smack.util.stringencoder.Base64;
import org.jivesoftware.smackx.ikey.IkeySignatureCreator;
import org.jivesoftware.smackx.ikey.IkeySignatureVerifier;
import org.jivesoftware.smackx.ikey.mechanism.IkeySignatureCreationMechanism;
import org.jivesoftware.smackx.ikey.mechanism.IkeySignatureVerificationMechanism;
import org.jivesoftware.smackx.ikey.mechanism.IkeyType;
import org.jivesoftware.smackx.ikey_ox.OxIkeySignatureCreationMechanism;
import org.jivesoftware.smackx.ikey_ox.OxIkeySignatureVerificationMechanism;
import org.jivesoftware.smackx.util.MercurySmackTestSuite;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.pgpainless.PGPainless;
import org.pgpainless.algorithm.KeyFlag;
import org.pgpainless.key.collection.PGPKeyRing;
import org.pgpainless.key.generation.KeySpec;
import org.pgpainless.key.generation.type.ECDSA;
import org.pgpainless.key.generation.type.curve.EllipticCurve;
import org.pgpainless.key.protection.UnprotectedKeysProtector;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class XepTest extends MercurySmackTestSuite {

    @Test
    public void test() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, PGPException, IOException, URISyntaxException {
        EntityBareJid jid = JidCreate.entityBareFromOrThrowUnchecked("juliet@capulet.lit");
        PGPKeyRing keyRing = PGPainless.generateKeyRing()
                .withMasterKey(KeySpec.getBuilder(ECDSA.fromCurve(EllipticCurve._P256))
                        .withKeyFlags(KeyFlag.CERTIFY_OTHER, KeyFlag.SIGN_DATA)
                        .withDefaultAlgorithms())
                .withPrimaryUserId("xmpp:" + jid)
                .withoutPassphrase()
                .build();

        IkeySignatureCreationMechanism signingMechanism = new OxIkeySignatureCreationMechanism(
                keyRing.getSecretKeys(), new UnprotectedKeysProtector());
        IkeySignatureCreator creator = new IkeySignatureCreator(signingMechanism);
        IkeySignatureVerificationMechanism verificationMechanism = new OxIkeySignatureVerificationMechanism(keyRing.getPublicKeys());
        IkeySignatureVerifier verifier = new IkeySignatureVerifier(verificationMechanism);

        SuperordinateElement superordinate = new SuperordinateElement(Base64.encodeToString(keyRing.getMasterKey().getEncoded()));

        List<SubordinateElement> subList = new ArrayList<>();
        subList.add(new SubordinateElement(
                "urn:xmpp:openpgp:0",
                new URI("xmpp:" + jid + "?;node=urn:xmpp:openpgp:0:public-keys:1357B01865B2503C18453D208CAC2A9678548E35;item=2020-01-21T10:46:21Z"),
                "1357B01865B2503C18453D208CAC2A9678548E35"));
        subList.add(new SubordinateElement(
                "urn:xmpp:omemo:1",
                new URI("xmpp:" + jid + "?;node=urn:xmpp:omemo:1:bundles;item=31415"),
                "e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f"));
        subList.add(new SubordinateElement(
                "eu.siacs.conversations.axolotl",
                new URI("xmpp:" + jid + "?;node=eu.siacs.conversations.axolotl.bundles:31415"),
                "e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f"));

        SubordinateListElement subs = new SubordinateListElement(jid, new Date(), subList);

        ProofElement proofElement = creator.createProofFor(subs);
        IkeyElement ikeyElement = new IkeyElement(IkeyType.OX, superordinate, new SignedElement(subs), proofElement);

        System.out.println(subs.toXML().toString());
        System.out.println(ikeyElement.toXML().toString());

        assertTrue(verifier.verify(ikeyElement, jid));
    }
}
