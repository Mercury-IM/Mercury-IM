package org.mercury_im.messenger.core.exception;

import org.jxmpp.jid.Jid;

import lombok.Getter;

public class ContactAlreadyAddedException extends Exception {

    @Getter
    private final Jid jid;

    public ContactAlreadyAddedException(Jid jid) {
        super("Contact with address " + jid.toString() + " is already a contact.");
        this.jid = jid;
    }
}
