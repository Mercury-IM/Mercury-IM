package org.mercury_im.messenger.android.ui.openpgp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import org.mercury_im.messenger.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OxSecretKeyBackupRestoreFragment extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.backup_code)
    EditText backupCode;

    @BindView(R.id.btn_scan)
    ImageButton scanButton;

    @BindView(R.id.btn_cancel)
    Button cancelButton;

    @BindView(R.id.btn_continue)
    Button continueButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ox_restore_backup, container, false);
        ButterKnife.bind(this, view);

        scanButton.setOnClickListener(v -> Toast.makeText(getContext(), R.string.not_yet_implemented, Toast.LENGTH_SHORT).show());


        return view;
    }
}
