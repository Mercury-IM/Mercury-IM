package org.mercury_im.messenger.data.repository;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.core.data.repository.AccountRepository;
import org.mercury_im.messenger.core.data.repository.PeerRepository;
import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.data.mapping.PeerMapping;
import org.mercury_im.messenger.data.model.PeerModel;
import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.entity.contact.Peer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.query.ResultDelegate;
import io.requery.reactivex.ReactiveEntityStore;

public class RxPeerRepository
        extends RequeryRepository
        implements PeerRepository {

    private final AccountRepository accountRepository;

    private final PeerMapping peerMapping;

    @Inject
    public RxPeerRepository(ReactiveEntityStore<Persistable> data,
                            PeerMapping peerMapping, AccountRepository accountRepository) {
        super(data);
        this.peerMapping = peerMapping;
        this.accountRepository = accountRepository;
    }

    @Override
    public Single<Peer> insertPeer(Peer peer) {
        return data().insert(peerMapping.toModel(peer, new PeerModel()))
                .map(model -> peerMapping.toEntity(model, peer));
    }

    @Override
    public Observable<Optional<Peer>> observePeer(UUID peerId) {
        return data().select(PeerModel.class)
                .where(PeerModel.ID.eq(peerId))
                .get().observableResult()
                .map(result -> new Optional<>(peerMapping.toEntity(result.firstOrNull(), new Peer())));
    }

    @Override
    public Maybe<Peer> getPeer(UUID peerId) {
        return data().select(PeerModel.class)
                .where(PeerModel.ID.eq(peerId))
                .get().maybe()
                .map(model -> peerMapping.toEntity(model, new Peer()));
    }

    @Override
    public Observable<Optional<Peer>> observePeerByAddress(UUID accountId, EntityBareJid address) {
        return data().select(PeerModel.class)
                .where(PeerModel.ACCOUNT_ID.eq(accountId))
                .and(PeerModel.ADDRESS.eq(address.asEntityBareJidString()))
                .get().observableResult()
                .map(result -> new Optional<>(result.firstOrNull()))
                .map(peerMapping::toEntity);
    }

    @Override
    public Single<Peer> getOrCreatePeer(UUID accountId, EntityBareJid address) {
        return accountRepository.getAccount(accountId).toSingle()
                .flatMap(account -> getOrCreatePeer(account, address));
    }

    @Override
    public Maybe<Peer> getPeerByAddress(UUID accountId, EntityBareJid address) {
        return data().select(PeerModel.class)
                .where(PeerModel.ACCOUNT_ID.eq(accountId))
                .and(PeerModel.ADDRESS.eq(address.asEntityBareJidString()))
                .get().maybe()
                .map(peerMapping::toEntity);
    }

    @Override
    public Single<Peer> getOrCreatePeer(Account account, EntityBareJid address) {
        return getPeerByAddress(account, address)
                .switchIfEmpty(
                        Single.just(new Peer())
                                .map(peer -> {
                                    peer.setAccount(account);
                                    peer.setAddress(address.asEntityBareJidString());
                                    return peer;
                                })
                                .flatMap(this::upsertPeer));
    }

    @Override
    public Observable<List<Peer>> observeAllPeers() {
        return data().select(PeerModel.class)
                .orderBy(PeerModel.ADDRESS)
                .get().observableResult()
                .map(ResultDelegate::toList)
                .map(this::peerModelsToEntities);
    }

    private List<Peer> peerModelsToEntities(List<PeerModel> peerModels) {
        List<Peer> peerEntities = new ArrayList<>(peerModels.size());
        for (PeerModel model : peerModels) {
            peerEntities.add(peerMapping.toEntity(model));
        }
        return peerEntities;
    }

    @Override
    public Observable<List<Peer>> observeAllContactsOfAccount(UUID accountId) {
        return data().select(PeerModel.class)
                .where(PeerModel.ACCOUNT_ID.eq(accountId))
                //.and(isContact())
                .get().observableResult()
                .map(ResultDelegate::toList)
                .map(this::peerModelsToEntities);
    }

    @Override
    public Single<Peer> updatePeer(Peer peer) {
        // In order to update, we fetch the model, update it and write it back.
        return data().select(PeerModel.class)
                .where(PeerModel.ID.eq(peer.getId()))
                .get().maybe().toSingle()
                // write changes into model
                .map(model -> peerMapping.toModel(peer, model))
                .flatMap(data()::update)
                .map(model -> peerMapping.toEntity(model, peer));
    }

    @Override
    public Single<Peer> upsertPeer(Peer peer) {
        return data().select(PeerModel.class)
                .where(PeerModel.ID.eq(peer.getId()))
                .get().maybe()
                // if not exists, create
                .switchIfEmpty(Single.just(peer)
                        .map(peerMapping::toModel)
                        .flatMap(data()::insert))
                // write changes into fetched model
                .map(model -> peerMapping.toModel(peer, model))
                // write changed model back to db
                .flatMap(data()::update)
                .map(model -> peerMapping.toEntity(model, peer));
    }

    @Override
    public Completable deletePeer(Peer peer) {
        return data().delete(PeerModel.class)
                .where(PeerModel.ID.eq(peer.getId()))
                .get().single().ignoreElement();
    }

    @Override
    public Completable deletePeer(UUID accountId, EntityBareJid address) {
        return data().delete(PeerModel.class)
                .where(PeerModel.ACCOUNT_ID.eq(accountId)
                        .and(PeerModel.ADDRESS.eq(address.asEntityBareJidString())))
                .get().single().ignoreElement();
    }

    @Override
    public Completable deleteAllPeers(UUID accountId) {
        return data().delete(PeerModel.class)
                .where(PeerModel.ACCOUNT_ID.eq(accountId))
                .get().single().ignoreElement();
    }

    @Override
    public Observable<List<Peer>> findPeers(String query) {
        return data().select(PeerModel.class)
                .where(PeerModel.ADDRESS.like("%" + query + "%")
                        .or(PeerModel.NAME.like("%" + query + "%")))
                .get().observableResult()
                .map(ResultDelegate::toList)
                .map(this::peerModelsToEntities);
    }

    @Override
    public Observable<List<Peer>> findPeers(UUID accountId, String query) {
        return data().select(PeerModel.class)
                .where(PeerModel.ACCOUNT_ID.eq(accountId)
                        .and(PeerModel.ADDRESS.like("%" + query + "%")
                                .or(PeerModel.NAME.like("%" + query + "%"))))
                .get().observableResult()
                .map(ResultDelegate::toList)
                .map(this::peerModelsToEntities);
    }
}
