package org.mercury_im.messenger.core.data.repository;

import org.mercury_im.messenger.entity.caps.EntityCapsRecord;

import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface EntityCapsRepository {

    Observable<Map<String, EntityCapsRecord>> observeAllEntityCapsRecords();

    Observable<EntityCapsRecord> observeEntityCapsRecords();

    Maybe<EntityCapsRecord> maybeGetEntityCapsRecord(String nodeVer);

    Completable insertEntityCapsRecord(EntityCapsRecord entityCapsRecord);

    Single<Integer> deleteAllEntityCapsRecords();
}
