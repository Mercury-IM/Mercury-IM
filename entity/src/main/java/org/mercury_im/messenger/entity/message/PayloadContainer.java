package org.mercury_im.messenger.entity.message;

import org.mercury_im.messenger.entity.message.content.Payload;

import java.util.List;

import lombok.Data;

/**
 * Defines a certain set of {@link Payload Payloads} of a message.
 * A {@link PayloadContainer} can either be a plaintext container or an encrypted container and contains
 * {@link Payload Payloads}.
 *
 * A message may contain encrypted and unencrypted payloads. Those could then be represented by
 * two different {@link PayloadContainer PayloadContainers}.
 */
@Data
public class PayloadContainer {
    long id;
    List<Payload> messageContents;
}
