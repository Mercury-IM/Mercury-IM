package org.mercury_im.messenger.core.connection.message;

import org.mercury_im.messenger.entity.chat.Chat;

import java.util.UUID;

import io.reactivex.Single;

public interface MessageConsignor {

    Single<UUID> sendTextMessage(Chat chat, String body);
}
