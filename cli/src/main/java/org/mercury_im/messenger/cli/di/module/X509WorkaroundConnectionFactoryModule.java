package org.mercury_im.messenger.cli.di.module;

import org.mercury_im.messenger.cli.X509WorkaroundConnectionFactory;
import org.mercury_im.messenger.core.connection.XmppConnectionFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class X509WorkaroundConnectionFactoryModule {

    @Provides
    @Singleton
    static XmppConnectionFactory provideConnectionFactory() {
        return new X509WorkaroundConnectionFactory();
    }
}
