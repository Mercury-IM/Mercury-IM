package org.jivesoftware.smackx.signed.xep0285;

import org.bouncycastle.openpgp.PGPSignature;
import org.jivesoftware.smackx.signed.xep0285.element.DataElement;
import org.jivesoftware.smackx.signed.xep0285.element.PlainElement;
import org.jivesoftware.smackx.signed.xep0285.element.SignatureElement;
import org.jivesoftware.smackx.signed.xep0285.element.SignedElement;
import org.jivesoftware.smackx.signed.xep0285.signing.Signer;

import java.io.IOException;

public class EncapsulatingSignatureManager {

    public static SignedElement createSignedElement(PlainElement plainElement, Signer signer) throws IOException {
        DataElement dataElement = DataElement.fromPlainElement(plainElement);
        PGPSignature signature = signer.createSignature(dataElement.getUtf8Bytes());
        SignatureElement signatureElement = SignatureElement.fromBytes(signer.getAlgorithmName(signature), signature.getEncoded());
        return new SignedElement(signatureElement, dataElement);
    }
}
