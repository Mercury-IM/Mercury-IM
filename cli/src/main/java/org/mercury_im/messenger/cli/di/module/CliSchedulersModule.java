package org.mercury_im.messenger.cli.di.module;

import org.mercury_im.messenger.core.SchedulersFacade;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@Module
public class CliSchedulersModule {

    @Provides
    @Named(value = SchedulersFacade.SCHEDULER_IO)
    @Singleton
    static Scheduler provideDatabaseThread() {
        return Schedulers.io();
    }

    @Provides
    @Named(value = SchedulersFacade.SCHEDULER_UI)
    @Singleton
    static Scheduler provideUIThread() {
        return Schedulers.newThread();
    }

    @Provides
    @Named(value = SchedulersFacade.SCHEDULER_NEW_THREAD)
    static Scheduler provideNewThread() {
        return Schedulers.newThread();
    }
}
