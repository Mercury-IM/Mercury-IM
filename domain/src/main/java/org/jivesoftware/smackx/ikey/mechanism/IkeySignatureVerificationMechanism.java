package org.jivesoftware.smackx.ikey.mechanism;

import java.io.IOException;

public interface IkeySignatureVerificationMechanism {

    boolean isSignatureValid(byte[] data, byte[] signature) throws IOException;

    IkeyType getType();
}
