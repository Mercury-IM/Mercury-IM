package org.jivesoftware.smackx.signed.xep0285;

import org.jivesoftware.smack.parsing.SmackParsingException;
import org.jivesoftware.smack.xml.XmlPullParserException;
import org.jivesoftware.smackx.signed.xep0285.element.SignedElement;
import org.jivesoftware.smackx.signed.xep0285.provider.SignedElementProvider;
import org.jivesoftware.smackx.util.MercurySmackTestSuite;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class SignedElementTest extends MercurySmackTestSuite {

    @Test
    public void test() throws XmlPullParserException, IOException, SmackParsingException {
        String example3 =
                "<signed xmlns=\"urn:xmpp:signed:0\">" +
                "<signature algorithm=\"RSA-SHA1\">" +
                "DxbxIziY1C1Ytcxkj0IFLsfmDLMv96JMlMAQZ7jh49IbsOIPsxI2LyLmqhKH/994UXDJKQLHvLJz" +
                "gAmw8V2b+zmyZeItJzSmB+HHiLFVXkD2Dd4JfetsafsfIcB7uNWg0gAeiKrTHfFgiyEC/2WxwOj3" +
                "JUMRyQ9ykEPIzS0GZ/k=" +
                "</signature>" +
                "<data>" +
                "PHBsYWluIHhtbG5zPSJ1cm46eG1wcDpzaWduZWQ6MCIgdGltZXN0YW1wPSIyMDEwLTA2LTI5VDAy" +
                "OjE1OjIxLjAxMloiPgogIFBHMWxjM05oWjJVZ2VHMXNibk05SW1waFltSmxjanBqYkdsbGJuUWlJ" +
                "R1p5YjIwOUltcDFiR2xsZEVCallYQgogIDFiR1YwTG01bGRDOWlZV3hqYjI1NUlpQjBiejBpY205" +
                "dFpXOUFiVzl1ZEdWbmRXVXVibVYwSWlCMGVYQmxQUwogIEpqYUdGMElqNDhkR2h5WldGa1BtTTJN" +
                "emN6T0RJMExXRXpNRGN0TkRCa1pDMDRabVV3TFdKaFpEWmxOekk1TwogIFdGa01Ed3ZkR2h5WldG" +
                "a1BqeGliMlI1UGxkb1pYSmxabTl5WlNCaGNuUWdkR2h2ZFN3Z1VtOXRaVzgvUEM5aQogIGIyUjVQ" +
                "and2YldWemMyRm5aVDQ9CjwvcGxhaW4+Cg==" +
                "</data>" +
                "</signed>";

        SignedElement signedElement = new SignedElementProvider()
                .parse(getParser(example3));

        System.out.println(example3);
        System.out.println(signedElement.toXML());
    }
}
