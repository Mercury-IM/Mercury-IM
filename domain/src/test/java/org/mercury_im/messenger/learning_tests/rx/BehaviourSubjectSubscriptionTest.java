package org.mercury_im.messenger.learning_tests.rx;

import org.junit.jupiter.api.Test;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

import static org.junit.jupiter.api.Assertions.assertNull;

public class BehaviourSubjectSubscriptionTest {

    @Test
    public void test() throws InterruptedException {
        Observable<String> observable = Observable.just("One", "Two", "Three");
        BehaviorSubject<String> behaviorSubject = BehaviorSubject.create();

        observable
                .subscribeOn(Schedulers.io())
                .subscribe(behaviorSubject);
        behaviorSubject
                //.subscribeOn(Schedulers.io())
                .subscribe(System.out::println);

        Thread.sleep(100);

        String s = behaviorSubject.getValue();
        assertNull(s);
    }
}
