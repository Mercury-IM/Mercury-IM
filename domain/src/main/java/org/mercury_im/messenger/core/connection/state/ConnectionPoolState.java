package org.mercury_im.messenger.core.connection.state;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class ConnectionPoolState {

    private final Map<UUID, ConnectionState> connectionStates;

    public ConnectionPoolState() {
        this(new ConcurrentHashMap<>());
    }

    public ConnectionPoolState(Map<UUID, ConnectionState> connectionStates) {
        this.connectionStates = connectionStates;
    }

    public Map<UUID, ConnectionState> getConnectionStates() {
        return connectionStates;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        Iterator<ConnectionState> iterator = connectionStates.values().iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next().toString());
            if (iterator.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
