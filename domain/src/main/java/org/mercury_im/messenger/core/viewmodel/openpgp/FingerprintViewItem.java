package org.mercury_im.messenger.core.viewmodel.openpgp;

import org.jivesoftware.smackx.ox.store.definition.OpenPgpTrustStore;
import org.jxmpp.jid.EntityBareJid;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.Date;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class FingerprintViewItem {
    UUID accountId;
    EntityBareJid owner;
    OpenPgpV4Fingerprint fingerprint;
    Date modificationDate;
    Date fetchDate;
    OpenPgpTrustStore.Trust trusted;

    public boolean isTrusted() {
        return trusted == OpenPgpTrustStore.Trust.trusted;
    }
}
