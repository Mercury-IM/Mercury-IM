package org.mercury_im.messenger.core.connection.message;

import org.mercury_im.messenger.core.connection.MercuryConnectionManager;
import org.mercury_im.messenger.core.data.repository.MessageRepository;
import org.mercury_im.messenger.entity.chat.Chat;

public abstract class AbstractMessageConsignor implements MessageConsignor {

    protected final MessageComposer messageComposer;

    protected final MercuryConnectionManager connectionManager;
    protected final MessageRepository messageRepository;
    protected final Chat chat;

    public AbstractMessageConsignor(MessageComposer composer,
                                    MercuryConnectionManager connectionManager,
                                    MessageRepository messageRepository,
                                    Chat chat) {
        this.messageComposer = composer;
        this.connectionManager = connectionManager;
        this.messageRepository = messageRepository;
        this.chat = chat;
    }
}
