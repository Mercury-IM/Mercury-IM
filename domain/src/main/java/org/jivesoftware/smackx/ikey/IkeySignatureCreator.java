package org.jivesoftware.smackx.ikey;

import org.jivesoftware.smack.util.stringencoder.Base64;
import org.jivesoftware.smackx.ikey.element.ProofElement;
import org.jivesoftware.smackx.ikey.element.SignedElement;
import org.jivesoftware.smackx.ikey.element.SubordinateListElement;
import org.jivesoftware.smackx.ikey.mechanism.IkeySignatureCreationMechanism;

import java.io.IOException;

public class IkeySignatureCreator {

    private final IkeySignatureCreationMechanism signatureCreationMechanism;

    public IkeySignatureCreator(IkeySignatureCreationMechanism signingMechanism) {
        this.signatureCreationMechanism = signingMechanism;
    }

    public ProofElement createProofFor(SubordinateListElement subordinateListElement)
            throws IOException {
        byte[] utf8 = new SignedElement(subordinateListElement).getUtf8Bytes();
        byte[] signature = signatureCreationMechanism.createSignature(utf8);

        return new ProofElement(Base64.encodeToString(signature));
    }
}
