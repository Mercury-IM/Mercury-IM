package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.SchedulersFacade;
import org.mercury_im.messenger.core.data.repository.DirectChatRepository;
import org.mercury_im.messenger.core.data.repository.MessageRepository;
import org.mercury_im.messenger.core.data.repository.PeerRepository;
import org.mercury_im.messenger.core.store.message.MercuryMessageStore;
import org.mercury_im.messenger.core.store.message.MercuryMessageStoreFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RxMercuryMessageStoreFactoryModule {

    @Provides
    @Singleton
    static MercuryMessageStoreFactory provideMessageStoreFactory(PeerRepository peerRepository,
                                                                 DirectChatRepository directChatRepository,
                                                                 MessageRepository messageRepository,
                                                                 SchedulersFacade schedulers) {

        return account -> new MercuryMessageStore(account, peerRepository, directChatRepository,
                messageRepository, schedulers);
    }
}
