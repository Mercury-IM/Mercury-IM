package org.mercury_im.messenger.data.converter;

import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.impl.JidCreate;

import javax.annotation.Nullable;

import io.requery.Converter;

public class EntityFullJidConverter implements Converter<EntityFullJid, String> {

    @Override
    public Class<EntityFullJid> getMappedType() {
        return EntityFullJid.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable
    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(EntityFullJid value) {
        return value == null ? null : value.toString();
    }

    @Override
    public EntityFullJid convertToMapped(Class<? extends EntityFullJid> type, @Nullable String value) {
        return value == null ? null : JidCreate.entityFullFromOrThrowUnchecked(value);
    }
}
