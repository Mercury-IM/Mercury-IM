package org.mercury_im.messenger.core;

public interface ClientStateListener {

    void onClientInForeground();

    void onClientInBackground();
}
