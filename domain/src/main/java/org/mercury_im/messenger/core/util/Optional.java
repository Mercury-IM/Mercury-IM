package org.mercury_im.messenger.core.util;

/**
 * Since j.u.Optional is only available on Android since API lvl 24, we need this utility class.
 *
 * @param <T> type of wrapped object.
 */
public class Optional<T> {

    private final T item;

    public Optional() {
        this(null);
    }

    public Optional(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }

    public boolean isPresent() {
        return item != null;
    }
}
