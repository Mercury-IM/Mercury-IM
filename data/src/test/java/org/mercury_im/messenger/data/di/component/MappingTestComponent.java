package org.mercury_im.messenger.data.di.component;


import org.mercury_im.messenger.data.di.MappingModule;
import org.mercury_im.messenger.data.mapping.AccountMappingTest;
import org.mercury_im.messenger.data.mapping.EntityCapsMappingTest;
import org.mercury_im.messenger.data.mapping.PeerMappingTest;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = MappingModule.class)
@Singleton
public interface MappingTestComponent {

    void inject(AccountMappingTest test);

    void inject(PeerMappingTest test);

    void inject(EntityCapsMappingTest test);
}
