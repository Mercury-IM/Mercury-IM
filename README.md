# Messenger

Mercury-IM will be an XMPP messenger that aims to be portable.
Most of the core logic is pure Java, so the final goal is to make it as easy as possible
to add different UIs for different platforms on top of it.

## Developing:

* Mercury IM's development tries to follow architectural principles know from
[Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).
* The app is developed using the MVVM (Model View Viewmodel) pattern
* Components are wired together using Dependency Injection (DI) with [Dagger 2](https://dagger.dev)
* Data is persisted using the [requery](https://github.com/requery/requery) ORM framework

## Building

Since there are some [issues](https://stackoverflow.com/questions/61410231/android-studio-cannot-resolve-symbols-for-composite-includebuild-dependency-in-j)
with Android Studio and [composite builds](https://docs.gradle.org/current/userguide/composite_builds.html),
it is currently only possible to build Mercury-IM either using the Gradle command line tool (see below),
or by using Android Studio 3.5.3.

```
git clone <project-url>
cd <project-directory>
git submodule update --init --recursive
gradle assembleDebug
```

## FAQ

* I want to develop, but lots of `org.jivesoftware.smackx.*` classes cannot be found!
  * You forgot to type `git submodule init && git submodule update` as mentioned above
* It looks like I'm missing `org.mercury_im.messenger.data.*` classes???
  * In Android Studio select the `data` module and then click "Build -> Make Module 'data'".
