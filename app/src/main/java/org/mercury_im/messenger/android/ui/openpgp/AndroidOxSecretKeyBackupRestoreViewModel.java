package org.mercury_im.messenger.android.ui.openpgp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import org.mercury_im.messenger.android.ui.base.MercuryAndroidViewModel;
import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.core.viewmodel.openpgp.OxBackupRestoreError;
import org.mercury_im.messenger.core.viewmodel.openpgp.OxSecretKeyBackupRestoreViewModel;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AndroidOxSecretKeyBackupRestoreViewModel extends AndroidViewModel
        implements MercuryAndroidViewModel<OxSecretKeyBackupRestoreViewModel> {

    private static final Logger LOGGER = Logger.getLogger(AndroidOxSecretKeyBackupRestoreViewModel.class.getName());

    // @Inject
    OxSecretKeyBackupRestoreViewModel commonViewModel;

    private MutableLiveData<Optional<OxBackupRestoreError>> restoreError =
            new MutableLiveData<>(new Optional<>());

    public AndroidOxSecretKeyBackupRestoreViewModel(@NonNull Application application) {
        super(application);
        // MercuryImApplication.getApplication().getAppComponent().inject(this);

        addDisposable(getCommonViewModel().observeBackupRestoreError()
                .subscribe(opt -> restoreError.postValue(opt),
                        e -> LOGGER.log(Level.SEVERE, "Could not subscribe android view model to backup restore errors", e)));
    }

    @Override
    public OxSecretKeyBackupRestoreViewModel getCommonViewModel() {
        return commonViewModel;
    }

    public void onRestoreCodeEntered(String code) {
        getCommonViewModel().onRestoreCodeEntered(code);
    }
}
