package org.mercury_im.messenger.android.ui.account.login;

import android.app.Application;
import android.text.Spannable;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.ui.base.MercuryAndroidViewModel;
import org.mercury_im.messenger.android.ui.openpgp.OpenPgpV4FingerprintFormatter;
import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.core.viewmodel.ikey.IkeyInfoViewModel;

import java.util.UUID;

import javax.inject.Inject;

public class AndroidIkeyInfoViewModel extends AndroidViewModel implements MercuryAndroidViewModel<IkeyInfoViewModel> {

    private MutableLiveData<Spannable> fingerprint = new MutableLiveData<>();

    @Inject
    IkeyInfoViewModel commonViewModel;

    public AndroidIkeyInfoViewModel(@NonNull Application application) {
        super(application);
        ((MercuryImApplication) application).getAppComponent().inject(this);
    }

    public void init(UUID accountId) {
        getCommonViewModel().init(accountId);

        addDisposable(getCommonViewModel().getFingerprint()
                .filter(Optional::isPresent)
                .map(Optional::getItem)
                .map(OpenPgpV4FingerprintFormatter::formatOpenPgpV4Fingerprint)
                .subscribe(fingerprint::postValue));
    }

    public LiveData<Spannable> getFingerprint() {
        return fingerprint;
    }

    @Override
    public IkeyInfoViewModel getCommonViewModel() {
        return commonViewModel;
    }
}
