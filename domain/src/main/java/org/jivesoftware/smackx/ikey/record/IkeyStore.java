package org.jivesoftware.smackx.ikey.record;

import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.jivesoftware.smackx.ikey.element.IkeyElement;
import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;
import org.jxmpp.jid.EntityBareJid;

import java.io.IOException;

public interface IkeyStore {

    IkeyElement loadIkeyRecord(EntityBareJid jid) throws IOException;

    void storeIkeyRecord(EntityBareJid jid, IkeyElement record) throws IOException;

    PGPSecretKeyRing loadSecretKey();

    void storeSecretKey(PGPSecretKeyRing secretKey);

    OpenPgpSecretKeyBackupPassphrase loadBackupPassphrase();

    void storeBackupPassphrase(OpenPgpSecretKeyBackupPassphrase passphrase);
}
