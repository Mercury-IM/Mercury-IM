package org.mercury_im.messenger.android.ui.account.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import org.mercury_im.messenger.R;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IkeyKeyInfoFragment extends Fragment {

    @BindView(R.id.fingerprint)
    TextView fingerprint;

    @BindView(R.id.btn_backup_ikey)
    Button buttonBackupIkey;

    @BindView(R.id.btn_done)
    Button buttonDone;

    private final UUID accountId;

    private AndroidIkeyInfoViewModel viewModel;

    public IkeyKeyInfoFragment(UUID accountId) {
        this.accountId = accountId;
    }

    public static Fragment newInstance(UUID accountId) {
        return new IkeyKeyInfoFragment(accountId);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ikey_key_info, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.viewModel = new ViewModelProvider(this).get(AndroidIkeyInfoViewModel.class);
        viewModel.init(accountId);
        viewModel.getFingerprint().observe(this, f -> fingerprint.setText(f));

        buttonBackupIkey.setOnClickListener(v -> ((IkeySetupNavigator)getActivity()).generateIkeyBackup(accountId));
        buttonDone.setOnClickListener(v -> getActivity().finish());
    }
}
