package org.mercury_im.messenger.android.util;

import android.os.Bundle;

import java.util.UUID;

public class ArgumentUtils {

    public static UUID requireUUID(Bundle bundle, String key) {
        String uuidString = bundle.getString(key);
        if (uuidString == null) {
            throw new IllegalArgumentException("Missing required UUID '" + key + "' (as String)");
        }
        return UUID.fromString(uuidString);
    }
}
