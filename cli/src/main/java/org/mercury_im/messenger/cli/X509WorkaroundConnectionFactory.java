package org.mercury_im.messenger.cli;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;
import org.jxmpp.stringprep.XmppStringprepException;
import org.mercury_im.messenger.core.connection.XmppConnectionFactory;
import org.mercury_im.messenger.entity.Account;

public class X509WorkaroundConnectionFactory implements XmppConnectionFactory {
    private static final int CONNECTION_TIMEOUT = 30 * 1000;

    public AbstractXMPPConnection createConnection(Account account) {
        try {
            XMPPTCPConnectionConfiguration.Builder configBuilder =
                    XMPPTCPConnectionConfiguration.builder()
                            .setCustomX509TrustManager(new TLSUtils.AcceptAllTrustManager())
                            .setConnectTimeout(CONNECTION_TIMEOUT)
                            .setXmppAddressAndPassword(account.getAddress(), account.getPassword());
            if (account.getHost() != null) {
                configBuilder.setHost(account.getHost());
            }
            if (account.getPort() != 0) {
                configBuilder.setPort(account.getPort());
            }
            return new XMPPTCPConnection(configBuilder.build());
        } catch (XmppStringprepException e) {
            throw new AssertionError("Account has invalid address: " + account.getAddress(), e);
        }
    }
}
