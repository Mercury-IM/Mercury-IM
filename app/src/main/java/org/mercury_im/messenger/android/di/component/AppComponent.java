package org.mercury_im.messenger.android.di.component;

import org.mercury_im.messenger.android.MercuryImApplication;
import org.mercury_im.messenger.android.di.module.AndroidDatabaseModule;
import org.mercury_im.messenger.android.di.module.AndroidSchedulersModule;
import org.mercury_im.messenger.android.ui.account.detail.AndroidAccountDetailsViewModel;
import org.mercury_im.messenger.android.ui.account.login.AndroidIkeyInfoViewModel;
import org.mercury_im.messenger.android.ui.account.login.EnterAccountDetailsFragment;
import org.mercury_im.messenger.android.ui.account.login.AndroidIkeySetupViewModel;
import org.mercury_im.messenger.android.ui.contacts.AndroidContactListViewModel;
import org.mercury_im.messenger.android.crypto.ikey.AndroidIkeyBackupCreationViewModel;
import org.mercury_im.messenger.core.di.module.IkeyModule;
import org.mercury_im.messenger.core.di.module.OpenPgpModule;
import org.mercury_im.messenger.core.di.module.RxMercuryMessageStoreFactoryModule;
import org.mercury_im.messenger.core.di.module.RxMercuryRosterStoreFactoryModule;
import org.mercury_im.messenger.core.di.module.StanzaIdSourceFactoryModule;
import org.mercury_im.messenger.core.di.module.XmppTcpConnectionFactoryModule;
import org.mercury_im.messenger.core.viewmodel.account.detail.AccountDetailsViewModel;
import org.mercury_im.messenger.core.viewmodel.account.list.AccountListViewModel;
import org.mercury_im.messenger.core.viewmodel.chat.ChatViewModel;
import org.mercury_im.messenger.data.di.RepositoryModule;
import org.mercury_im.messenger.android.di.module.AppModule;
import org.mercury_im.messenger.core.di.module.ViewModelModule;
import org.mercury_im.messenger.android.service.MercuryForegroundService;
import org.mercury_im.messenger.core.store.caps.MercuryEntityCapsStore;
import org.mercury_im.messenger.android.ui.MainActivity;
import org.mercury_im.messenger.android.ui.account.list.AndroidAccountListViewModel;
import org.mercury_im.messenger.android.ui.account.login.AndroidLoginViewModel;
import org.mercury_im.messenger.android.ui.chat.ChatActivity;
import org.mercury_im.messenger.android.ui.chat.ChatInputFragment;
import org.mercury_im.messenger.android.ui.chat.ChatInputViewModel;
import org.mercury_im.messenger.android.ui.chat.AndroidChatViewModel;
import org.mercury_im.messenger.android.ui.chatlist.AndroidChatListViewModel;
import org.mercury_im.messenger.android.ui.contacts.detail.ContactDetailActivity;
import org.mercury_im.messenger.android.ui.contacts.detail.ContactDetailViewModel;
import org.mercury_im.messenger.core.viewmodel.account.LoginViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Main Application Component that binds together all the modules needed for the Android
 * application.
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
                AndroidDatabaseModule.class,
                AndroidSchedulersModule.class,
                RepositoryModule.class,
                ViewModelModule.class,
                XmppTcpConnectionFactoryModule.class,
                RxMercuryMessageStoreFactoryModule.class,
                OpenPgpModule.class,
                IkeyModule.class,
                RxMercuryRosterStoreFactoryModule.class,
                StanzaIdSourceFactoryModule.class
        })
public interface AppComponent {

    // Application

    void inject(MercuryImApplication mercuryImApplication);


    // Views

    void inject(MainActivity mainActivity);

    void inject(ChatActivity chatActivity);

    void inject(ChatInputFragment chatInputFragment);

    void inject(ContactDetailActivity contactDetailActivity);

    void inject(EnterAccountDetailsFragment enterAccountDetailsFragment);

    // ViewModels

    void inject(AndroidContactListViewModel contactListViewModel);

    void inject(AndroidChatViewModel androidChatViewModel);

    void inject(ChatViewModel chatViewModel);

    void inject(ChatInputViewModel chatInputViewModel);

    void inject(AndroidLoginViewModel androidLoginViewModel);

    void inject(AndroidAccountListViewModel androidAccountsViewModel);

    void inject(AndroidChatListViewModel chatListViewModel);

    void inject(ContactDetailViewModel contactDetailViewModel);

    void inject(AccountDetailsViewModel accountDetailsViewModel);

    void inject(AndroidAccountDetailsViewModel accountDetailsViewModel);

    void inject(AndroidIkeyBackupCreationViewModel androidIkeyBackupCreationViewModel);

    void inject(AndroidIkeySetupViewModel androidIkeySetupViewModel);
   // void inject(AndroidOxSecretKeyBackupRestoreViewModel androidOxSecretKeyBackupRestoreViewModel);

    void inject(AndroidIkeyInfoViewModel androidIkeyInfoViewModel);


                // Common VMs
    void inject(LoginViewModel loginViewModel);

    void inject(AccountListViewModel accountsViewModel);


    // Services

    void inject(MercuryForegroundService service);

    void inject(MercuryEntityCapsStore store);

}
