package org.jivesoftware.smackx.signed.xep0285.element;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.XmlEnvironment;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class SignedElement implements ExtensionElement {

    public static final String ELEMENT = "signed";

    private final SignatureElement signature;
    private final DataElement data;

    public SignedElement(SignatureElement signature, DataElement data) {
        this.signature = signature;
        this.data = data;
    }

    public SignatureElement getSignature() {
        return signature;
    }

    public DataElement getData() {
        return data;
    }

    @Override
    public String getNamespace() {
        return PlainElement.NAMESPACE;
    }

    @Override
    public String getElementName() {
        return ELEMENT;
    }

    @Override
    public XmlStringBuilder toXML(XmlEnvironment xmlEnvironment) {
        return new XmlStringBuilder(this)
                .rightAngleBracket()
                .append(getSignature())
                .append(getData())
                .closeElement(this);
    }
}
