package org.mercury_im.messenger.data.repository.dao;

import org.mercury_im.messenger.data.model.DirectChatModel;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class DirectChatDao extends RequeryDao {

    private static final Logger LOGGER = Logger.getLogger(DirectChatDao.class.getName());

    @Inject
    public DirectChatDao(ReactiveEntityStore<Persistable> data) {
        super(data);
    }

    public Single<DirectChatModel> insert(DirectChatModel chat) {
        LOGGER.log(Level.INFO, "Inserting " + chat.toString());
        return data().insert(chat);
    }

    public ReactiveResult<DirectChatModel> get(UUID chatId) {
        return data().select(DirectChatModel.class)
                .where(DirectChatModel.ID.eq(chatId))
                .get();
    }

    public ReactiveResult<DirectChatModel> getByPeer(UUID peerId) {
        return data().select(DirectChatModel.class)
                .where(DirectChatModel.PEER_ID.eq(peerId))
                .get();
    }

    public ReactiveResult<DirectChatModel> getAll() {
        return data().select(DirectChatModel.class)
                .get();
    }

    public Single<Integer> delete(UUID chatId) {
        return data().delete(DirectChatModel.class)
                .where(DirectChatModel.ID.eq(chatId))
                .get()
                .single();
    }
}
