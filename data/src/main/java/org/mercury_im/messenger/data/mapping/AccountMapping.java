package org.mercury_im.messenger.data.mapping;

import org.mercury_im.messenger.data.model.AccountModel;
import org.mercury_im.messenger.entity.Account;

import javax.inject.Inject;

public class AccountMapping extends AbstractMapping<Account, AccountModel> {

    @Inject
    public AccountMapping() {

    }

    @Override
    public Account newEntity(AccountModel model) {
        return new Account();
    }

    @Override
    public AccountModel newModel(Account entity) {
        return new AccountModel();
    }

    @Override
    public AccountModel mapToModel(Account entity, AccountModel model) {
        model.setId(entity.getId());
        model.setAddress(entity.getAddress());
        model.setPassword(entity.getPassword());
        model.setHost(entity.getHost());
        model.setPort(entity.getPort());
        model.setEnabled(entity.isEnabled());
        model.setRosterVersion(entity.getRosterVersion());

        return model;
    }

    @Override
    public Account mapToEntity(AccountModel model, Account entity) {
        entity.setId(model.getId());
        entity.setAddress(model.getAddress());
        entity.setPassword(model.getPassword());
        entity.setHost(model.getHost());
        entity.setPort(model.getPort());
        entity.setEnabled(model.isEnabled());
        entity.setRosterVersion(model.getRosterVersion());

        return entity;
    }
}
