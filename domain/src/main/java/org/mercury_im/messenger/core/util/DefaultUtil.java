package org.mercury_im.messenger.core.util;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.pgpainless.key.OpenPgpV4Fingerprint;

public class DefaultUtil {

    public static OpenPgpV4Fingerprint defaultFingerprint() {
        return new OpenPgpV4Fingerprint("0123456789ABCDEF0123456789ABCDEF01234567");
    }

    public static EntityBareJid defaultJid() {
        return JidCreate.entityBareFromOrThrowUnchecked("placeholder@example.org");
    }
}
