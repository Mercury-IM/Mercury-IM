package org.mercury_im.messenger.cli;

import java.io.Console;

public class ConsoleUserInterface implements UserInterface {

    public static boolean isSupported() {
        return System.console() != null;
    }

    @Override
    public String readText() {
        Console console = System.console();
        return console.readLine();
    }

    @Override
    public String readPassword() {
        return new String(System.console().readPassword());
    }
}
