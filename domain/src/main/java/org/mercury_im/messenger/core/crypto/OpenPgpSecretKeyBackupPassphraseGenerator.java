package org.mercury_im.messenger.core.crypto;

import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;

public interface OpenPgpSecretKeyBackupPassphraseGenerator {

    OpenPgpSecretKeyBackupPassphrase generateBackupPassphrase();
}
