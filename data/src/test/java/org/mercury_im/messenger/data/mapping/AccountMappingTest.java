package org.mercury_im.messenger.data.mapping;

import org.junit.jupiter.api.Test;
import org.mercury_im.messenger.data.di.component.DaggerMappingTestComponent;
import org.mercury_im.messenger.data.model.AccountModel;
import org.mercury_im.messenger.entity.Account;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AccountMappingTest {

    @Inject
    AccountMapping accountMapping;

    public static final Account ACCOUNT_MISSION_CONTROL;
    public static final Account ACCOUNT_LITTLE_JOE;

    static {
        ACCOUNT_MISSION_CONTROL = new Account();
        ACCOUNT_MISSION_CONTROL.setAddress("mission-controll@planet.earth");
        ACCOUNT_MISSION_CONTROL.setEnabled(true);
        ACCOUNT_MISSION_CONTROL.setPassword("notBecauseTheyAreEasy");

        ACCOUNT_LITTLE_JOE = new Account();
        ACCOUNT_LITTLE_JOE.setAddress("little-joe@planet.earth");
        ACCOUNT_LITTLE_JOE.setEnabled(false);
        ACCOUNT_LITTLE_JOE.setPassword("butBecauseTheyAreHard");
    }

    public AccountMappingTest() {
        DaggerMappingTestComponent.create().inject(this);
    }

    @Test
    public void entityToModel() {

        AccountModel model = accountMapping.toModel(ACCOUNT_MISSION_CONTROL);

        assertEquals(ACCOUNT_MISSION_CONTROL.getId(), model.getId());
        assertEquals(ACCOUNT_MISSION_CONTROL.getAddress(), model.getAddress());
        assertEquals(ACCOUNT_MISSION_CONTROL.getPassword(), model.getPassword());
        assertEquals(ACCOUNT_MISSION_CONTROL.isEnabled(), model.isEnabled());
    }

    @Test
    public void modelToEntity() throws NoSuchFieldException, IllegalAccessException {
        AccountModel model = new AccountModel();
        model.getId();
        model.setAddress("model@entity.store");
        model.setEnabled(true);
        model.setPassword("12345");

        Account entity = accountMapping.toEntity(model);

        assertEquals(model.getId(), entity.getId());
        assertEquals(model.getAddress(), entity.getAddress());
        assertEquals(model.getPassword(), entity.getPassword());
    }
}
