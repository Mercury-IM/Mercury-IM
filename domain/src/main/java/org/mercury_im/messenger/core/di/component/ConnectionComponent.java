package org.mercury_im.messenger.core.di.component;

import java.util.UUID;

import dagger.BindsInstance;
import dagger.Component;

@Component
public interface ConnectionComponent {

    ConnectionComponent getComponent();

    @Component.Builder
    interface Builder {

        @BindsInstance Builder forAccount(@Account UUID accountId);

        ConnectionComponent build();

    }
}
