package org.mercury_im.messenger.entity.contact;

import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.mercury_im.messenger.entity.Account;

import java.util.List;
import java.util.UUID;

import lombok.Data;

/**
 * Defines a user on the network (eg. a contact, chat partner, group chat member etc).
 * Basically anyone that may send you a message is a Peer.
 */
@Data
public class Peer {
    UUID id;
    Account account;
    String address;
    String name;
    SubscriptionDirection subscriptionDirection;
    boolean subscriptionPending;
    boolean subscriptionApproved;
    List<String> groupNames;

    public Peer() {
        this.id = UUID.randomUUID();
    }

    public String getDisplayName() {
        if (name != null && !name.trim().isEmpty()) {
            return name;
        }
        return getJid().getLocalpart().asUnescapedString();
    }

    public EntityBareJid getJid() {
        return JidCreate.entityBareFromOrThrowUnchecked(getAddress());
    }
}
