package org.jivesoftware.smackx.signed.xep0285.verification;

import org.jivesoftware.smackx.signed.xep0285.element.PlainElement;

public interface TimestampValidator {

    boolean isValid(PlainElement plainElement);

    void set(PlainElement plainElement);

    default boolean compareAndSet(PlainElement plainElement) {
        if (isValid(plainElement)) {
            set(plainElement);
            return true;
        }
        return false;
    }
}
