package org.mercury_im.messenger.core.listener;

import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.entity.message.Message;

public interface IncomingDirectMessageListener {

    void onIncomingDirectMessage(Account account, DirectChat chat, Message message);

}
