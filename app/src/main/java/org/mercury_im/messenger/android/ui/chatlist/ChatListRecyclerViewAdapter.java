package org.mercury_im.messenger.android.ui.chatlist;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.recyclerview.widget.RecyclerView;

import org.jivesoftware.smack.util.Objects;
import org.mercury_im.messenger.R;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.android.ui.avatar.AvatarDrawable;
import org.mercury_im.messenger.android.ui.chat.ChatActivity;
import org.mercury_im.messenger.android.ui.util.AbstractRecyclerViewAdapter;

import java.util.logging.Level;
import java.util.logging.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatListRecyclerViewAdapter
        extends AbstractRecyclerViewAdapter<DirectChat, ChatListRecyclerViewAdapter.ChatHolder> {

    public ChatListRecyclerViewAdapter() {
        super(new ChatMessageDiffCallback(true));
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_chat, parent, false);
        return new ChatHolder(parent.getContext(), view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatHolder holder, int position) {
        DirectChat model = getItemAt(position);
        String name = model.getPeer().getDisplayName();
        String address = model.getPeer().getAddress();
        holder.nameView.setText(name);
        holder.avatarView.setImageDrawable(new AvatarDrawable(name, address));
        holder.itemView.setOnClickListener(view -> {

            Intent intent = new Intent(holder.context, ChatActivity.class);
            intent.putExtra(ChatActivity.EXTRA_CHAT_ID, model.getId().toString());

            holder.context.startActivity(intent);
        });

        holder.itemView.setOnLongClickListener(v -> {
            ((AppCompatActivity) v.getContext()).startSupportActionMode(actionModeCallback);
            return true;
        });
    }

    public static class ChatHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.chat_name)
        TextView nameView;

        @BindView(R.id.chat_message)
        TextView chatMessage;

        @BindView(R.id.chat_time)
        TextView chatTime;

        @BindView(R.id.chat_avatar)
        ImageView avatarView;

        Context context;

        public ChatHolder(Context context, @NonNull View itemView) {
            super(itemView);
            this.context = context;
            ButterKnife.bind(this, itemView);
        }
    }

    private static class ChatMessageDiffCallback extends AbstractDiffCallback<DirectChat> {

        ChatMessageDiffCallback(boolean detectMoves) {
            super(detectMoves);
        }

        @Override
        public boolean areItemsTheSame(DirectChat oldItem, DirectChat newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(DirectChat oldItem, DirectChat newItem) {
            return Objects.equals(oldItem.getPeer().getName(), newItem.getPeer().getName());
        }
    }

    private androidx.appcompat.view.ActionMode.Callback actionModeCallback = new androidx.appcompat.view.ActionMode.Callback() {
        private boolean multiSelect = false;

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.actionmode_chatlist, menu);
            multiSelect = true;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            mode.finish();

            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            notifyDataSetChanged();
        }
    };
}
