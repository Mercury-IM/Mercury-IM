package org.mercury_im.messenger.data.converter;

import org.mercury_im.messenger.entity.message.MessageDirection;

import io.requery.Converter;

public class MessageDirectionConverter implements Converter<MessageDirection, Integer> {
    @Override
    public Class<MessageDirection> getMappedType() {
        return MessageDirection.class;
    }

    @Override
    public Class<Integer> getPersistedType() {
        return Integer.class;
    }

    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public Integer convertToPersisted(MessageDirection value) {
        switch (value) {
            case outgoing:
                return 0;
            case incoming:
                return 1;
            default: return 1;
        }
    }

    @Override
    public MessageDirection convertToMapped(Class<? extends MessageDirection> type, Integer value) {
        switch (value) {
            case 0:
                return MessageDirection.outgoing;
            case 1:
                return MessageDirection.incoming;
            default: return MessageDirection.incoming;
        }
    }
}
