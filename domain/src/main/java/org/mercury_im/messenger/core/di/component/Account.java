package org.mercury_im.messenger.core.di.component;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@java.lang.annotation.Documented
@java.lang.annotation.Retention(RUNTIME)
@javax.inject.Qualifier
public @interface Account {

}
