package org.mercury_im.messenger.core.di.module;

import org.jivesoftware.smackx.ikey.IkeyManager;
import org.jivesoftware.smackx.ox.OpenPgpManager;
import org.mercury_im.messenger.core.connection.MercuryConnection;
import org.mercury_im.messenger.core.connection.MercuryConnectionManager;
import org.mercury_im.messenger.core.di.component.Account;

import java.util.UUID;

import dagger.Module;
import dagger.Provides;

@Module
public class MercuryConnectionModule {

    @Provides
    @Account
    MercuryConnection provideConnection(MercuryConnectionManager connectionManager, @Account UUID accountId) {
        MercuryConnection connection = connectionManager.getConnection(accountId);
        if (connection == null) {
            connection = connectionManager.createConnection(accountId);
            connectionManager.doRegisterConnection(connection);
        }
        return connection;
    }

    @Provides
    @Account
    OpenPgpManager provideOpenPgpManager(MercuryConnection connection) {
        return OpenPgpManager.getInstanceFor(connection.getConnection());
    }

    @Provides
    @Account
    IkeyManager provideIkeyManager(MercuryConnection connection) {
        return IkeyManager.getInstanceFor(connection.getConnection());
    }
}
