package org.mercury_im.messenger.data.model;

import java.util.UUID;

import io.requery.CascadeAction;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.ForeignKey;
import io.requery.Generated;
import io.requery.Key;
import io.requery.OneToOne;
import io.requery.Persistable;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Entity
@Table(name = "chats")
public abstract class AbstractDirectChatModel implements Persistable {

    @Key
    @Convert(UUIDConverter.class)
    UUID id;

    @OneToOne(cascade = CascadeAction.NONE)
    @ForeignKey(referencedColumn = "id")
    PeerModel peer;

    boolean displayed;
}
