package org.mercury_im.messenger.data.model;

import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.data.converter.EntityBareJidConverter;
import org.mercury_im.messenger.data.converter.OpenPgpV4FingerprintConverter;
import org.pgpainless.key.OpenPgpV4Fingerprint;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.requery.CascadeAction;
import io.requery.Column;
import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Index;
import io.requery.Key;
import io.requery.OneToMany;
import io.requery.Table;
import io.requery.converter.UUIDConverter;

@Table(name = "ikey_record", uniqueIndexes = {"record_index"})
@Entity
public class AbstractIkeyRecordModel {

    @Key
    UUID id;

    @Column(name = "account", unique = true)
    @Index("record_index")
    @Convert(UUIDConverter.class)
    UUID accountId;

    @Column(name = "jid", unique = true)
    @Index("record_index")
    @Convert(EntityBareJidConverter.class)
    EntityBareJid jid;

    @Column(name = "\"timestamp\"")
    Date timestamp;

    @OneToMany(mappedBy = "record", cascade = {CascadeAction.DELETE, CascadeAction.SAVE})
    @Column
    List<IkeySubordinateModel> subordinates;

    @Column(name = "superordinate")
    PGPPublicKeyRing superordinate;

    @Column(name = "fingerprint")
    @Convert(OpenPgpV4FingerprintConverter.class)
    OpenPgpV4Fingerprint fingerprint;

    @Column(name = "trusted")
    boolean trusted;

    @Column(name = "proof")
    String proof;

}
