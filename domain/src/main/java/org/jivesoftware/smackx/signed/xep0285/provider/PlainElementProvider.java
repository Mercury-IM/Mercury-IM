package org.jivesoftware.smackx.signed.xep0285.provider;

import org.jivesoftware.smack.packet.XmlEnvironment;
import org.jivesoftware.smack.parsing.SmackParsingException;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.util.ParserUtils;
import org.jivesoftware.smack.xml.XmlPullParser;
import org.jivesoftware.smack.xml.XmlPullParserException;
import org.jivesoftware.smackx.signed.xep0285.element.PlainElement;

import java.io.IOException;

public class PlainElementProvider extends ExtensionElementProvider<PlainElement> {
    @Override
    public PlainElement parse(XmlPullParser parser, int initialDepth, XmlEnvironment xmlEnvironment)
            throws XmlPullParserException, IOException, SmackParsingException {
        String timestamp = ParserUtils.getRequiredAttribute(parser, PlainElement.ATTR_TIMESTAMP);
        return PlainElement.fromBase64String(parser.nextText(), ParserUtils.getDateFromXep82String(timestamp));
    }
}
