package org.mercury_im.messenger.data.converter;

import org.jivesoftware.smackx.ox.store.definition.OpenPgpTrustStore;

import io.requery.converter.EnumStringConverter;

public class OpenPgpTrustConverter extends EnumStringConverter<OpenPgpTrustStore.Trust> {
    public OpenPgpTrustConverter() {
        super(OpenPgpTrustStore.Trust.class);
    }
}
