package org.mercury_im.messenger.core.crypto;

public class OxPlusIkeyKeyGenerationStrategy implements LocalOxKeyGenerationStrategy {

    @Override
    public boolean promptForBackupRestoreIfNoLocalKeyPresent() {
        return false;
    }
}
