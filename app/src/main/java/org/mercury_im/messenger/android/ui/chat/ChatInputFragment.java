package org.mercury_im.messenger.android.ui.chat;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import org.mercury_im.messenger.R;

public class ChatInputFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.text_body)
    EditText textInput;

    @BindView(R.id.btn_media)
    ImageButton addAttachment;

    @BindView(R.id.btn_send)
    ImageButton buttonSend;

    @BindView(R.id.btn_emoji)
    ImageButton buttonEmoji;

    private ChatInputViewModel mViewModel;

    private OnChatInputActionListener actionListener;

    public static ChatInputFragment newInstance() {
        return new ChatInputFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_compose, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ChatInputViewModel.class);
        observeViewModel(mViewModel);
    }

    /**
     * Observe the {@link ChatInputViewModel} for changes and reflect those in the UI.
     *
     * @param viewModel ViewModel
     */
    private void observeViewModel(ChatInputViewModel viewModel) {
        // viewModel.getDraft().observe(ChatInputFragment.this, draft -> textInput.setText(draft));
    }

    /**
     * Attach this fragment to the parent {@link android.app.Activity} which MUST implement
     * {@link OnChatInputActionListener} and is bound to handle input actions like send button
     * clicked etc.
     *
     * @param context parent activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChatInputActionListener) {
            this.actionListener = (OnChatInputActionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAccountListItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.actionListener = null;
    }

    @Override
    @OnClick({R.id.btn_send, R.id.btn_media, R.id.btn_emoji})
    public void onClick(View view) {
        switch (view.getId()) {
            // Add media
            case R.id.btn_media:
                if (actionListener != null) {
                    actionListener.onButtonMediaClicked();
                }
                break;

            // Send message
            case R.id.btn_send:
                if (actionListener != null) {
                    actionListener.onComposingBodySend(textInput.getText().toString());
                }
                textInput.setText(null);
                break;

            case R.id.btn_emoji:
                if (actionListener != null) {
                    actionListener.onButtonEmojiClicked();
                }
                break;
        }
    }

    public interface OnChatInputActionListener {

        void onButtonMediaClicked();

        void onButtonEmojiClicked();

        void onComposingBodyChanged(String body);

        void onComposingBodySend(String body);
    }
}
