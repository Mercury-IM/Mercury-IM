package org.mercury_im.messenger.data.di;

import org.mercury_im.messenger.data.mapping.AccountMapping;
import org.mercury_im.messenger.data.mapping.DirectChatMapping;
import org.mercury_im.messenger.data.mapping.EntityCapsMapping;
import org.mercury_im.messenger.data.mapping.GroupChatMapping;
import org.mercury_im.messenger.data.mapping.MessageMapping;
import org.mercury_im.messenger.data.mapping.PeerMapping;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MappingModule {

    @Provides
    @Singleton
    static AccountMapping provideAccountMapping() {
        return new AccountMapping();
    }

    @Provides
    @Singleton
    static PeerMapping providePeerMapping(AccountMapping accountMapping) {
        return new PeerMapping(accountMapping);
    }

    @Provides
    @Singleton
    static DirectChatMapping provideDirectChatMapping(PeerMapping peerMapping) {
        return new DirectChatMapping(peerMapping);
    }

    @Provides
    @Singleton
    static GroupChatMapping provideGroupChatMapping(AccountMapping accountMapping) {
        return new GroupChatMapping(accountMapping);
    }

    @Provides
    @Singleton
    static MessageMapping provideMessageMapping() {
        return new MessageMapping();
    }

    @Provides
    @Singleton
    static EntityCapsMapping provideEntityCapsMapping() {
        return new EntityCapsMapping();
    }
}
