package org.mercury_im.messenger.data.converter;

import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;

import javax.annotation.Nullable;

import io.requery.Converter;

public class OpenPGPSecretKeyBackupPassphraseConverter
        implements Converter<OpenPgpSecretKeyBackupPassphrase, String> {
    @Override
    public Class<OpenPgpSecretKeyBackupPassphrase> getMappedType() {
        return OpenPgpSecretKeyBackupPassphrase.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable
    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(OpenPgpSecretKeyBackupPassphrase value) {
        return value == null ? null : value.toString();
    }

    @Override
    public OpenPgpSecretKeyBackupPassphrase convertToMapped(Class<? extends OpenPgpSecretKeyBackupPassphrase> type, @Nullable String value) {
        return value == null ? null : new OpenPgpSecretKeyBackupPassphrase(value);
    }
}
