package org.mercury_im.messenger.core.listener;

import org.mercury_im.messenger.entity.chat.Chat;
import org.mercury_im.messenger.entity.event.TypingEvent;

public interface TypingEventListener {

    void onTypingEventReceived(Chat chat, TypingEvent typingEvent);
}
