package org.jivesoftware.smackx.signed.xep0285.signing;

import org.bouncycastle.openpgp.PGPSignature;

import java.io.IOException;

public interface Signer {

    PGPSignature createSignature(byte[] data) throws IOException;

    String getAlgorithmName(PGPSignature signature);
}
