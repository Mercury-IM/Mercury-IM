package org.mercury_im.messenger.entity.message;

public enum MessageDeliveryState {
    pending_delivery,
    delivered_to_server,
    delivered_to_peer,
    read,
    delivery_error,
    ;
}
