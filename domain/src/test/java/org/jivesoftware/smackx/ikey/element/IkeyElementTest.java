package org.jivesoftware.smackx.ikey.element;

import org.jivesoftware.smack.parsing.SmackParsingException;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smack.xml.XmlPullParserException;
import org.jivesoftware.smackx.ikey.mechanism.IkeyType;
import org.jivesoftware.smackx.ikey.provider.IkeyElementProvider;
import org.jivesoftware.smackx.util.MercurySmackTestSuite;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class IkeyElementTest extends MercurySmackTestSuite {

    @Test
    public void elementTest() throws URISyntaxException, XmlPullParserException, IOException, SmackParsingException {
        IkeyType type = IkeyType.OX;
        Date date = new Date();
        SuperordinateElement superordinate = new SuperordinateElement("" +
                "VGhpcyBpcyBvbmx5IGEgdGVzdCwgbm90IGEgcmVhbCBrZXkuIFdoeSB3b3VsZCB5b3UgdGhpbmsg" +
                "dGhpcyBpcyBhIHJlYWwga2V5PyBCZWNhdXNlIGl0IGlzICdiYXNlNjQtZW5jcnlwdGVkJz8gRHVt" +
                "bXkgZHVtYiBkdW1iLg==");
        SubordinateListElement subordinates = buildSubListElement(
                JidCreate.entityBareFromOrThrowUnchecked("hamlet@denmark.lit"),
                date,
                new SubordinateElement(
                        "urn:xmpp:openpgp:0",
                        new URI("xmpp:hamlet@denmark.lit?;node=urn:xmpp:openpgp:0:public-keys:1357B01865B2503C18453D208CAC2A9678548E35;item=2020-01-21T10:46:21Z"),
                        "1357B01865B2503C18453D208CAC2A9678548E35"),
                new SubordinateElement(
                        "urn:xmpp:omemo:1",
                        new URI("xmpp:hamlet@denmark.lit?;node=urn:xmpp:omemo:1:bundles;item=123456"),
                        "e64dc9166dd34db64c9247bd502c5969e365a98f3aa41c87247d120487fdd32f")
        );
        ProofElement proof = new ProofElement("d2hpbGUgdGhpcyBpcyBub3QgYSB2YWxpZCBwcm9vZiwgaXQgaXMgc3VmZmljaWVudCBmb3IgdGVzdGluZy4=");

        IkeyElement ikeyElement = new IkeyElement(type, superordinate, new SignedElement(subordinates), proof);
        String xml = ikeyElement.toXML().toString();
        System.out.println(xml);
        System.out.println(subordinates.toXML().toString());

        IkeyElement parsed = IkeyElementProvider.INSTANCE.parse(PacketParserUtils.getParserFor(xml));

        assertEquals(ikeyElement, parsed);
        assertEquals(subordinates, parsed.getSignedElement().getChildElement());
    }

    private static SubordinateListElement buildSubListElement(EntityBareJid jid, Date date, SubordinateElement... subordinateElements) {
        return new SubordinateListElement(jid, date, Arrays.asList(subordinateElements));
    }

}
