package org.jivesoftware.smackx.ikey.mechanism;

import java.io.IOException;

public interface IkeySignatureCreationMechanism {

    byte[] createSignature(byte[] data) throws IOException;

    IkeyType getType();

}
