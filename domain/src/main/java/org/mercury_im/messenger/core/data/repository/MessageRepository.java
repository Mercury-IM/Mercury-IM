package org.mercury_im.messenger.core.data.repository;

import org.jxmpp.jid.EntityBareJid;
import org.mercury_im.messenger.entity.chat.DirectChat;
import org.mercury_im.messenger.entity.chat.GroupChat;
import org.mercury_im.messenger.entity.message.ChatMarkerState;
import org.mercury_im.messenger.entity.message.Message;
import org.mercury_im.messenger.entity.message.MessageDeliveryState;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface MessageRepository {

    Single<Message> insertMessage(Message message);

    Observable<List<Message>> observeMessages(DirectChat chat);

    Observable<List<Message>> observeMessages(GroupChat chat);

    Observable<List<Message>> findMessagesWithBody(String body);

    Observable<List<Message>> findMessagesWithBody(DirectChat chat, String body);

    Observable<List<Message>> findMessagesWithBody(GroupChat chat, String body);

    Single<Message> upsertMessage(Message message);

    Single<Message> updateMessage(Message message);

    Completable deleteMessage(Message message);

    Completable markMessage(String stanzaId, EntityBareJid xmppAddressOfChatPartner, ChatMarkerState received);

    Completable updateDeliveryState(UUID messageId, MessageDeliveryState deliveryState);
}
