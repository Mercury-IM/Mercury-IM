package org.mercury_im.messenger.core.connection.message;

import org.jivesoftware.smack.packet.id.StandardStanzaIdSource;
import org.mercury_im.messenger.entity.chat.Chat;
import org.mercury_im.messenger.entity.message.Message;
import org.mercury_im.messenger.entity.message.MessageDeliveryState;
import org.mercury_im.messenger.entity.message.MessageDirection;

import java.util.Date;
import java.util.UUID;

public class MessageComposer {

    public Message createTextMessage(Chat chat, String body) {
        UUID messageId = UUID.randomUUID();
        Message message = new Message();
        message.setId(messageId);
        message.setChatId(chat.getId());

        message.setLegacyStanzaId(new StandardStanzaIdSource().getNewStanzaId());
        message.setOriginId(messageId.toString());

        message.setBody(body);
        message.setSender(chat.getAccount().getJid());
        message.setRecipient(chat.getJid());
        message.setDeliveryState(MessageDeliveryState.pending_delivery);
        message.setDirection(MessageDirection.outgoing);
        message.setRead(false);
        message.setTimestamp(new Date());

        return message;
    }
}
