package org.mercury_im.messenger.core.data.repository;

import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.jivesoftware.smackx.ox.OpenPgpSecretKeyBackupPassphrase;
import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.entity.Account;

import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface IkeyKeyRepository {

    default Observable<Optional<PGPSecretKeyRing>> loadSecretKey(Account account) {
        return loadSecretKey(account.getId());
    }

    Observable<Optional<PGPSecretKeyRing>> loadSecretKey(UUID accountId);

    default Completable storeSecretKey(Account account, PGPSecretKeyRing secretKey) {
        return storeSecretKey(account.getId(), secretKey);
    }

    Completable storeSecretKey(UUID accountId, PGPSecretKeyRing secretKey);

    default Single<Integer> deleteSecretKey(Account account) {
        return deleteSecretKey(account.getId());
    }

    Single<Integer> deleteSecretKey(UUID accountId);

    default Single<Optional<OpenPgpSecretKeyBackupPassphrase>> loadBackupPassphrase(Account account) {
        return loadBackupPassphrase(account.getId());
    }

    Single<Optional<OpenPgpSecretKeyBackupPassphrase>> loadBackupPassphrase(UUID accountID);

    default Completable storeBackupPassphrase(Account account, OpenPgpSecretKeyBackupPassphrase passphrase) {
        return storeBackupPassphrase(account.getId(), passphrase);
    }

    Completable storeBackupPassphrase(UUID accountID, OpenPgpSecretKeyBackupPassphrase passphrase);
}
