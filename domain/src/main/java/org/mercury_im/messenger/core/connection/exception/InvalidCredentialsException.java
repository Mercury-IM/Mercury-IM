package org.mercury_im.messenger.core.connection.exception;

public class InvalidCredentialsException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }
}
