package org.mercury_im.messenger.data.repository.dao;

import org.mercury_im.messenger.data.model.GroupChatModel;

import java.util.UUID;

import javax.inject.Inject;

import io.reactivex.Single;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

public class GroupChatDao extends RequeryDao {

    @Inject
    public GroupChatDao(ReactiveEntityStore<Persistable> data) {
        super(data);
    }

    public Single<GroupChatModel> insert(GroupChatModel chat) {
        return data().insert(chat);
    }

    public ReactiveResult<GroupChatModel> get(UUID chatId) {
        return data().select(GroupChatModel.class)
                .where(GroupChatModel.ID.eq(chatId))
                .get();
    }

    public ReactiveResult<GroupChatModel> get(UUID accountId, String address) {
        return data().select(GroupChatModel.class)
                .where(GroupChatModel.ACCOUNT_ID.eq(accountId))
                .and(GroupChatModel.ADDRESS.eq(address))
                .get();
    }

    public ReactiveResult<GroupChatModel> getAll() {
        return data().select(GroupChatModel.class)
                .get();
    }

    public Single<Integer> delete(UUID chatId) {
        return data().delete(GroupChatModel.class)
                .where(GroupChatModel.ID.eq(chatId))
                .get()
                .single();
    }
}
