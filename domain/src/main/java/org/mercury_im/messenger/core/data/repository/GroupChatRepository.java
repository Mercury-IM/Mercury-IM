package org.mercury_im.messenger.core.data.repository;

import org.mercury_im.messenger.core.util.Optional;
import org.mercury_im.messenger.entity.Account;
import org.mercury_im.messenger.entity.chat.GroupChat;

import java.util.List;
import java.util.UUID;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface GroupChatRepository {

    Single<GroupChat> insertGroupChat(GroupChat chat);

    Observable<Optional<GroupChat>> observeGroupChat(UUID chatId);

    Maybe<GroupChat> getGroupChat(UUID chatId);

    Single<GroupChat> getOrCreateGroupChat(Account account, String roomAddress);

    default Observable<Optional<GroupChat>> observeGroupChatByRoomAddress(Account account, String roomAddress) {
        return observeGroupChatByRoomAddress(account.getId(), roomAddress);
    }

    Observable<Optional<GroupChat>> observeGroupChatByRoomAddress(UUID accountId, String roomAddress);

    default Maybe<GroupChat> getGroupChatByRoomAddress(Account account, String roomAddress) {
        return getGroupChatByRoomAddress(account.getId(), roomAddress);
    }

    Maybe<GroupChat> getGroupChatByRoomAddress(UUID accountId, String roomAddress);

    Observable<List<GroupChat>> observeAllGroupChats();

    default Observable<List<GroupChat>> observeAllGroupChatsOfAccount(Account account) {
        return observeAllGroupChatsOfAccount(account.getId());
    }

    Observable<List<GroupChat>> observeAllGroupChatsOfAccount(UUID accountId);

    Single<GroupChat> updateGroupChat(GroupChat chat);

    Single<GroupChat> upsertGroupChat(GroupChat chat);

    default Completable deleteGroupChat(GroupChat chat) {
        return deleteGroupChat(chat.getId());
    }

    Completable deleteGroupChat(UUID chatId);

}
