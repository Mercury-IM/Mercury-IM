package org.mercury_im.messenger.android.crypto.ikey;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import org.mercury_im.messenger.R;

public class IkeyBackupRestoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ikey_backup_restore);
    }
}
