package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.SchedulersFacade;
import org.mercury_im.messenger.core.data.repository.AccountRepository;
import org.mercury_im.messenger.core.data.repository.PeerRepository;
import org.mercury_im.messenger.core.store.roster.MercuryRosterStore;
import org.mercury_im.messenger.core.store.roster.MercuryRosterStoreFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RxMercuryRosterStoreFactoryModule {

    @Provides
    @Singleton
    static MercuryRosterStoreFactory provideRosterStoreFactory(PeerRepository peerRepository, AccountRepository accountRepository, SchedulersFacade schedulers) {
        return accountId -> new MercuryRosterStore(accountId, peerRepository, accountRepository, schedulers);
    }
}
