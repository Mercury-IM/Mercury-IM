package org.mercury_im.messenger.core.account.error;

public enum UsernameError {
    emptyUsername,
    invalidUsername,
    unreachableServer
}
