package org.mercury_im.messenger.core.di.module;

import org.mercury_im.messenger.core.crypto.LocalOxKeyGenerationStrategy;
import org.mercury_im.messenger.core.crypto.OxPlusIkeyKeyGenerationStrategy;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class IkeyModule {

    @Singleton
    @Provides
    static LocalOxKeyGenerationStrategy provideLocalOxKeyGenerationStrategy() {
        return new OxPlusIkeyKeyGenerationStrategy();
    }
}
