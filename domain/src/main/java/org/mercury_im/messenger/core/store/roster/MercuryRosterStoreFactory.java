package org.mercury_im.messenger.core.store.roster;

import java.util.UUID;

public interface MercuryRosterStoreFactory {

    MercuryRosterStore createRosterStore(UUID accountId);
}
