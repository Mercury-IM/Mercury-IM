package org.mercury_im.messenger.entity.message;

import org.jxmpp.jid.EntityJid;
import org.mercury_im.messenger.entity.Encryption;

import java.util.Date;
import java.util.UUID;

import lombok.Data;

@Data
public class Message {
    UUID id;
    UUID chatId;
    EntityJid sender;
    EntityJid recipient;

    String body;

    Date timestamp;
    MessageDirection direction;
    MessageDeliveryState deliveryState;

    // <message id="blabla"/>
    String legacyStanzaId;
    // XEP-0359: Unique and Stable Stanza IDs
    String stanzaId;
    String originId;

    // Serialized message
    String xml;

    Encryption encryption;
    boolean received;
    boolean read;
    boolean pending;

    public boolean isIncoming() {
        return getDirection() == MessageDirection.incoming;
    }

    public Message() {
        this.id = UUID.randomUUID();
    }
}
